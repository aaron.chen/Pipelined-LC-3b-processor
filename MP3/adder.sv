//out = a1 + a0
module adder #(parameter width = 16)
(
	input [width-1:0] a1, a0,
	output logic [width-1:0] out
);

always_comb
begin
	out = a1 + a0;
end
	
endmodule : adder