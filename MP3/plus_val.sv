// out = in + val
//should be used for PC + 2
module plus_val #(parameter width = 16, val = 2)
(
	input logic [width-1:0] in,
	output logic [width-1:0] out
);

assign out = in + val;

endmodule : plus_val