import lc3b_types::*;

module mem_stall
(
	input logic clk,
	input lc3b_opcode opcode,
	input logic resp_b,
	
	output logic stall
);

logic [1:0] stall_count;
logic [1:0] stall_next;

initial
begin
	stall_count = 2'b00;
end

always_comb
begin
	stall_next = stall_count;
	stall = 1'b0;

	if ((opcode == op_ldi) || (opcode == op_sti))
	begin
		// if (stall_count == 2'b11)
		// begin
		// 	stall_next = 2'b00;
		// 	stall = 1'b0;
		// end
		// else if (resp_b && stall_count == 2'b10)
		// begin
		// 	stall_next = 2'b11;
		// 	stall = 1'b1;
		// end
		// else if (resp_b && stall_count == 2'b01)
		// begin
		// 	stall_next = 2'b10;
		// 	stall = 1'b1;
		// end
		// else if (resp_b && stall_count == 2'b00)
		// begin
		// 	stall_next = 2'b01;
		// 	stall = 1'b1;
		// end
		// else if (stall_count == 2'b00)
		// begin
		// 	stall_next = 2'b01;
		// 	stall = 1'b0;
		// end
		// else
		// begin
		// 	stall_next = stall_count;
		// 	stall = 1'b1;
		// end
		if (stall_count == 2'b10)
		begin
			stall_next = 2'b00;
			stall = 1'b0;
		end
		else if (resp_b && stall_count == 2'b01)
		begin
			stall_next = 2'b10;
			stall = 1'b1;
		end
		else if (resp_b && stall_count == 2'b00)
		begin
			stall_next = 2'b01;
			stall = 1'b1;
		end
		else
		begin
			stall_next = 2'b00;
			stall = 1'b1;
		end
	end
	else if ((opcode == op_ldr) || (opcode == op_str) || (opcode == op_ldb) || (opcode == op_stb))
	begin
		// if (stall_count == 2'b10)
		// begin
		// 	stall_next = 2'b00;
		// 	stall = 1'b0;
		// end
		// else if (resp_b && stall_count == 2'b01)
		// begin
		// 	stall_next = 2'b10;
		// 	stall = 1'b1;
		// end
		// else if (resp_b && stall_count == 2'b00)
		// begin
		// 	stall_next = 2'b01;
		// 	stall = 1'b1;
		// end
		// else if (stall_count == 2'b00)
		// begin
		// 	stall_next = 2'b01;
		// 	stall = 1'b0;
		// end
		// else
		// begin
		// 	stall_next = stall_count;
		// 	stall = 1'b1;
		// end
		if (stall_count == 2'b01)
		begin
			stall_next = 2'b00;
			stall = 1'b0;
		end
		else if (resp_b && stall_count == 2'b00)
		begin
			stall_next = 2'b01;
			stall = 1'b1;
		end
		else
		begin
			stall_next = 2'b00;
			stall = 1'b1;
		end
	end
	else
	begin
		stall_next = 2'b00;
		stall = 1'b0;
	end
end

always @(posedge clk)
begin
	stall_count <= stall_next;
end

endmodule : mem_stall