import lc3b_types::*;

/*
 * ZEXT[trapvector] << 1
 */
//takes 8-bit trap vector, left shifts it, zexts it to 16-bit
module trapzext
(
	input lc3b_trap8 in,
	output lc3b_word out
);

assign out = $unsigned({in, 1'b0});

endmodule : trapzext