import lc3b_types::*;

module datapath
(
	input clk,

	/* Port A (PC stage)*/
	output logic read_a,
	output logic write_a,
	output logic [1:0] wmask_a,
	output logic [15:0] address_a,
	output logic [15:0] wdata_a,
	input logic resp_a,
	input logic [15:0] rdata_a,

	/* Port B (MEM stage)*/
	output logic read_b,
	output logic write_b,
	output logic [1:0] wmask_b,
	output logic [15:0] address_b,
	output logic [15:0] wdata_b,
	input logic resp_b,
	input logic [15:0] rdata_b
);

/******* INTERNAL SIGNALS ******/
//CTRL WORD GENERATION
lc3b_control_word ctrl_rom_out;
//FETCH
lc3b_word pcmux_out;
lc3b_word pc_out;
lc3b_word pc_plus2_out;
lc3b_word instr_data_out;
logic [1:0] pcmux_sel_bp_out;
logic [1:0] pcmux_sel_squash;
//FETCH <-> DECODE
lc3b_control_word IF_ID_ctrl_out;
lc3b_reg IF_ID_dr_out;
lc3b_reg IF_ID_sr1_out;
lc3b_reg IF_ID_sr2_out;
lc3b_imm5 IF_ID_imm5_out;
lc3b_imm4 IF_ID_imm4_out;
lc3b_offset11 IF_ID_off11_out;
lc3b_offset9 IF_ID_off9_out;
lc3b_offset6 IF_ID_off6_out;
lc3b_trap8 IF_ID_trap8_out;
lc3b_word IF_ID_pc_out;
//DECODE
logic load_regfilemux_out;
lc3b_reg storemux_out;
lc3b_reg drmux_out;
lc3b_word regfile_sr1_out;
lc3b_word regfile_sr2_out;
lc3b_word imm5sext_out;
lc3b_word imm4sext_out;
lc3b_word off11shiftsext_out;
lc3b_word off9shiftsext_out;
lc3b_word off6shiftsext_out;
lc3b_word off6sext_out;
lc3b_word operand1mux_out;
lc3b_word operand2mux_out;
lc3b_word inmux_out;
//DECODE <-> EXECUTE
lc3b_control_word ID_EX_ctrl_out;
lc3b_word ID_EX_operand1_out;
lc3b_word ID_EX_operand2_out;
lc3b_word ID_EX_regfile_sr_out;
lc3b_reg ID_EX_rega_out;
lc3b_reg ID_EX_regb_out;
lc3b_reg ID_EX_dr_nzp_out;
lc3b_trap8 ID_EX_trap8_out;
//EXECUTE
logic [1:0] fwd1mux_sel;
logic [1:0] fwd2mux_sel;
lc3b_word fwd1mux_out;
lc3b_word fwd2mux_out;
lc3b_word alu_out;
//EXECUTE <-> MEMORY
lc3b_control_word EX_MEM_ctrl_out;
lc3b_word EX_MEM_alu_out;
lc3b_word EX_MEM_regfile_sr_out;
lc3b_reg EX_MEM_dr_nzp_out;
lc3b_trap8 EX_MEM_trap8_out;
//MEMORY ACCESS
lc3b_word alu_out_regfile_sr_outmux_out;
lc3b_word mdrmux_out;
lc3b_word alu_out_mem_outmux_out;
lc3b_word marmux_out;
lc3b_word trapzext_out;
lc3b_word stb_data;
lc3b_reg cc_out;
logic cccomp_out;
logic stall;
logic [1:0] gen_pcmux_sel_out;
lc3b_word mem_data_out;
lc3b_mem_wmask stb_wmask;
logic squash;
//MEMORY ACCESS <-> WRITEBACK
lc3b_control_word MEM_WB_ctrl_out;
lc3b_word MEM_WB_alu_out;
lc3b_word MEM_WB_mem_out;
lc3b_reg MEM_WB_dr_nzp_out;
//WRITEBACK
lc3b_byte bytemux_out;
lc3b_word bytezext_out;
lc3b_word datawritemux_out;
lc3b_reg gencc_out;

//******* FETCH STAGE *********
//internal wires / assignments

mux2to1 #(.width(2)) pcmux_sel_bp // branch prediction
(
	.sel((ctrl_rom_out == op_br)),
	.a0(gen_pcmux_sel_out),
	.a1(2'b00),
	.out(pcmux_sel_bp_out)
);

mux2to1 #(.width(2)) pc_squashmux
(
	.sel(squash),
	.a0(pcmux_sel_bp_out),
	.a1(2'b01),
	.out(pcmux_sel_squash)
);

//will probably have to add extra sel bit for jump
//sel 0 -> PC+2 -> PC
//sel 1 -> PC + off -> PC
mux3to1 pcmux
(
	.sel(pcmux_sel_squash),
	.a0(pc_plus2_out),
	.a1(EX_MEM_alu_out),
	.a2(datawritemux_out),
	.out(pcmux_out)
);

//register to hold pc value
//(assumes quick mem resp and load every clk cycle) **CHANGE IN CP2**
register pc_reg
(
	.clk,
//	.load(1'b1),
	.load((stall == 1'b0) && (resp_a == 1'b1) || gen_pcmux_sel_out == 2'b01), // stall the pipeline if indirect_state_out is 1
	.in(pcmux_out),
	.out(pc_out)
);

//get PC + 2
plus_val #(.val(2)) pc_plus2
(
	.in(pc_out),
	.out(pc_plus2_out)
);

//get data at requested PC address
//temporary "block" to handle PC memory access at port A
//(assumes quick mem resp and load every clk cycle) **CHANGE IN CP2**
assign read_a = true;  //read pc addr
assign write_a = false; //don't overwrite pc addr
assign wmask_a = 2'b11; //get all 16 bits
assign address_a = pc_out; //request pc addr
assign wdata_a = 16'hzzzz; //write value to first pc addr (don't care)
//assume resp_a always 1, and rdata_a is fetched immediately
assign instr_data_out = rdata_a;

//generate control word for IF_ID stage
control_rom ctrl_rom
(
	.opcode(lc3b_opcode'(instr_data_out[15:12])),
	.shf_a(instr_data_out[5]), // also used as the imm sel
	.shf_d(instr_data_out[4]),
	.jsr_sel(instr_data_out[11]),
	.ctrl(ctrl_rom_out)
);

//******** FETCH <-> DECODE *********

//(assumes quick mem resp and load every clk cycle) **CHANGE IN CP2**
IF_ID_register IF_ID
(
	.clk,
//	.load(1'b1),
	.load((stall == 1'b0) && (resp_a == 1'b1)), // stall the pipeline if indirect_state_out is 1
	.squash,
	.ctrl_in(ctrl_rom_out),
	.instr_in(instr_data_out),
	.pc_in(pc_plus2_out),
	.ctrl_out(IF_ID_ctrl_out),
	.dr_out(IF_ID_dr_out),
	.sr1_out(IF_ID_sr1_out),
	.sr2_out(IF_ID_sr2_out),
	.imm5_out(IF_ID_imm5_out),
	.imm4_out(IF_ID_imm4_out),
	.off11_out(IF_ID_off11_out),
	.off9_out(IF_ID_off9_out),
	.off6_out(IF_ID_off6_out),
	.trap8_out(IF_ID_trap8_out),
	.pc_out(IF_ID_pc_out)
);

//******* DECODE STAGE *********

mux2to1 #(.width(3)) storemux
(
	.sel(IF_ID_ctrl_out.storemux_sel),
	.a0(IF_ID_sr2_out),
	.a1(IF_ID_dr_out),
	.out(storemux_out)
);

mux2to1 #(.width(3)) drmux
(
	.sel(MEM_WB_ctrl_out.drmux_sel),
	.a0(MEM_WB_dr_nzp_out),
	.a1(3'b111), // R7
	.out(drmux_out)
);

mux2to1 inmux
(
	.sel(MEM_WB_ctrl_out.inmux_sel),
	.a0(datawritemux_out),
	.a1(IF_ID_pc_out),
	.out(inmux_out)
);

mux2to1 #(.width(1)) load_regfilemux
(
	.sel((stall == 1'b0) && MEM_WB_ctrl_out.load_regfilemux_sel),
	.a0(MEM_WB_ctrl_out.load_regfile && (MEM_WB_ctrl_out.opcode != op_jsr) && (MEM_WB_ctrl_out.opcode != op_trap)), // TODO: make this logic into a separate module for clarity
	.a1(IF_ID_ctrl_out.load_regfile),
	.out(load_regfilemux_out)
);

regfile regfile
(
	.clk,
//	.load(load_regfilemux_out),
	.load((load_regfilemux_out == 1'b1) && (stall == 1'b0)),
	.in(inmux_out),
	.src_a(IF_ID_sr1_out),
	.src_b(storemux_out),
	.dest(drmux_out),
	.reg_a(regfile_sr1_out),
	.reg_b(regfile_sr2_out)
);

imm #(.width(5)) imm5sext
(
	.in(IF_ID_imm5_out),
	.out(imm5sext_out)
);

imm #(.width(4)) imm4sext
(
	.in(IF_ID_imm4_out),
	.out(imm4sext_out)
);

adj #(.width(11)) off11shiftsext
(
	.in(IF_ID_off11_out),
	.out(off11shiftsext_out)
);

adj #(.width(9)) off9shiftsext
(
	.in(IF_ID_off9_out),
	.out(off9shiftsext_out)
);

adj #(.width(6)) off6shiftsext
(
	.in(IF_ID_off6_out),
	.out(off6shiftsext_out)
);

sext #(.width(6)) off6sext
(
	.in(IF_ID_off6_out),
	.out(off6sext_out)
);

mux2to1 operand1mux
(
	.sel(IF_ID_ctrl_out.operand1mux_sel),
	.a0(regfile_sr1_out),
	.a1(IF_ID_pc_out),
	.out(operand1mux_out)
);

mux8to1 operand2mux
(
	.sel(IF_ID_ctrl_out.operand2mux_sel),
	.a0(regfile_sr2_out),
	.a1(imm5sext_out),
	.a2(imm4sext_out),
	.a3(off11shiftsext_out),
	.a4(off9shiftsext_out),
	.a5(off6shiftsext_out),
	.a6(off6sext_out),
	.a7({12'b000000000000,IF_ID_imm4_out}),
	.out(operand2mux_out)
);

//******** DECODE <-> EXECUTE *********
//(assumes quick mem resp and load every clk cycle) **CHANGE IN CP2**
ID_EX_register ID_EX
(
	.clk,
//	.load(1'b1),
	.load((stall == 1'b0)), // stall the pipeline if indirect_state_out is 1
	.squash,
	.ctrl_in(IF_ID_ctrl_out),

	.operand1_in(operand1mux_out), 
	.operand2_in(operand2mux_out), 
	.regfile_sr_in(regfile_sr2_out), // for use in str
	.dr_nzp_in(IF_ID_dr_out), //for use in ldr or in branch comp
	.trap8_in(IF_ID_trap8_out),
	.rega_in(IF_ID_sr1_out),
	.regb_in(storemux_out),

	.ctrl_out(ID_EX_ctrl_out),
	.operand1_out(ID_EX_operand1_out), 
	.operand2_out(ID_EX_operand2_out), 
	.regfile_sr_out(ID_EX_regfile_sr_out), // for use in str
	.dr_nzp_out(ID_EX_dr_nzp_out), //for use in ldr or in branch comp
	.trap8_out(ID_EX_trap8_out),
	.rega_out(ID_EX_rega_out),
	.regb_out(ID_EX_regb_out)
);

//******* EXECUTE STAGE *********

fwd_ctrl fwd_ctrl
(
	.ID_EX_rega(ID_EX_rega_out),
	.ID_EX_regb(ID_EX_regb_out),
	.EX_MEM_dr(EX_MEM_dr_nzp_out),
	.EX_MEM_load_regfile(EX_MEM_ctrl_out.load_regfile),
	.MEM_WB_dr(MEM_WB_dr_nzp_out),
	.MEM_WB_load_regfile(MEM_WB_ctrl_out.load_regfile),
	.fwd1mux_sel,
	.fwd2mux_sel
);

mux3to1 fwd1mux
(
	.sel(fwd1mux_sel),
	.a0(ID_EX_operand1_out),
	.a1(datawritemux_out),
	.a2(EX_MEM_alu_out),
	.out(fwd1mux_out)
);

mux3to1 fwd2mux
(
	.sel(fwd2mux_sel),
	.a0(ID_EX_operand2_out),
	.a1(datawritemux_out),
	.a2(EX_MEM_alu_out),
	.out(fwd2mux_out)
);

alu alu
(
	.aluop(ID_EX_ctrl_out.aluop),
	.a(fwd1mux_out),
	.b(fwd2mux_out),
	.f(alu_out)
);

//******** EXECUTE <-> MEMORY *********
//(assumes quick mem resp and load every clk cycle) **CHANGE IN CP2**
EX_MEM_register EX_MEM
(
	.clk,
//	.load(1'b1),
	.load((stall == 1'b0)), // stall the pipeline if indirect_state_out is 1
	.squash,
	.ctrl_in(ID_EX_ctrl_out),

	.alu_in(alu_out), 
	.regfile_sr_in(ID_EX_regfile_sr_out), // for use in str
	.dr_nzp_in(ID_EX_dr_nzp_out), //for use in ldr or in branch comp
	.trap8_in(ID_EX_trap8_out),
	
	.ctrl_out(EX_MEM_ctrl_out),
	.alu_out(EX_MEM_alu_out), 
	.regfile_sr_out(EX_MEM_regfile_sr_out), // for use in str
	.dr_nzp_out(EX_MEM_dr_nzp_out), //for use in br
	.trap8_out(EX_MEM_trap8_out)
);

//******* MEMORY ACCESS STAGE *********

stb_calc stb_calc
(
	.in(EX_MEM_regfile_sr_out),
	.addr_lsb(EX_MEM_alu_out[0]),
	.stb_data,
	.stb_wmask
);

mem_stall mem_stall
(
	.clk,
	.opcode(EX_MEM_ctrl_out.opcode),
	.resp_b,
	.stall(stall)
);

mux2to1 #(.width(2)) wmaskmux
(
	.sel(EX_MEM_ctrl_out.wmaskmux_sel),
	.a0(2'b11),
	.a1(stb_wmask),
	.out(wmask_b)
);

// TODO: give this a better name
mux2to1 alu_out_regfile_sr_outmux 
(
	.sel(stall),
	.a0(EX_MEM_alu_out),
	.a1(EX_MEM_regfile_sr_out),
	.out(alu_out_regfile_sr_outmux_out)
);

mux4to1 mdrmux
(
	.sel(EX_MEM_ctrl_out.mdrmux_sel),
	.a0(alu_out_regfile_sr_outmux_out),
	.a1(EX_MEM_regfile_sr_out),
	.a2(mem_data_out),
	.a3(stb_data),
//	.a3({8'b00000000,EX_MEM_regfile_sr_out[7:0]}),
//	.a4({EX_MEM_regfile_sr_out[7:0],8'b00000000}),
	.out(mdrmux_out)
);

trapzext trapzext
(
	.in(EX_MEM_trap8_out),
	.out(trapzext_out)
);

// TODO: give this a better name
mux2to1 alu_out_mem_outmux 
(
	.sel((EX_MEM_ctrl_out.opcode == op_ldi) || (EX_MEM_ctrl_out.opcode == op_sti)),
	.a0(EX_MEM_alu_out),
	.a1(MEM_WB_mem_out),
	.out(alu_out_mem_outmux_out)
);

mux2to1 marmux
(
	.sel(EX_MEM_ctrl_out.marmux_sel),
	.a0(alu_out_mem_outmux_out),
	.a1(trapzext_out),
	.out(marmux_out)
);

register #(.width(3)) cc
(
	.clk,
//	.load(MEM_WB_ctrl_out.load_cc),
	.load((MEM_WB_ctrl_out.load_cc == 1'b1)),
	.in(gencc_out),
	.out(cc_out)
);

cccomp cccomp
(
	.nzp_query(EX_MEM_dr_nzp_out),
	.nzp_status(cc_out),
	.to_branch(cccomp_out)
);

gen_pcmux_sel gen_pcmux_sel
(
	.EX_MEM_opcode(EX_MEM_ctrl_out.opcode),
	.MEM_WB_opcode(MEM_WB_ctrl_out.opcode),
	.cccomp(cccomp_out),
	.pcmux_sel(gen_pcmux_sel_out)
);

//get data at requested memory address
//temporary "block" to handle memory access at port B
//(assumes quick mem resp and load every clk cycle) **CHANGE IN CP2**

assign read_b = EX_MEM_ctrl_out.read_b || (stall == 1'b1 && MEM_WB_ctrl_out.opcode == op_ldi);
assign write_b = EX_MEM_ctrl_out.write_b || (stall == 1'b1 && MEM_WB_ctrl_out.opcode == op_sti);
//assign wmask_b = EX_MEM_ctrl_out.wmask_b;
assign address_b = marmux_out;
assign wdata_b = mdrmux_out;
assign mem_data_out = rdata_b;

// branch prediction
assign squash = ((EX_MEM_ctrl_out.opcode == op_br) && (cccomp_out == 1'b1));

//******** MEMORY <-> WRITEBACK *********
//(assumes quick mem resp and load every clk cycle) **CHANGE IN CP2**
MEM_WB_register MEM_WB
(
	.clk,
	.load(1'b1),
	// .load((stall == 1'b0)), // stall the pipeline if indirect_state_out is 1
	.squash,
	.ctrl_in(EX_MEM_ctrl_out),
	.resp_b(resp_b),

	.alu_in(EX_MEM_alu_out), 
	.mem_in(mem_data_out),
	.dr_nzp_in(EX_MEM_dr_nzp_out), //for use in ldr or in branch comp

	.ctrl_out(MEM_WB_ctrl_out),
	.alu_out(MEM_WB_alu_out),
	.mem_out(MEM_WB_mem_out),
	.dr_nzp_out(MEM_WB_dr_nzp_out) //for use in br
);

//******* WRITEBACK STAGE *********

mux2to1 #(.width(8)) bytemux
(
	.sel(MEM_WB_alu_out[0]),
	.a0(MEM_WB_mem_out[15:8]),
	.a1(MEM_WB_mem_out[7:0]),
	.out(bytemux_out)
);

bytezext bytezext
(
	.in(bytemux_out),
	.out(bytezext_out)
);

mux3to1 datawritemux
(
	.sel(MEM_WB_ctrl_out.datawritemux_sel),
	.a0(MEM_WB_alu_out),
	.a1(MEM_WB_mem_out),
	.a2(bytezext_out),
	.out(datawritemux_out)
);

gencc gencc
(
	.in(datawritemux_out),
	.out(gencc_out)
);

endmodule : datapath