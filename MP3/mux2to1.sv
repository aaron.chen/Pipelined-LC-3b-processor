//out = a1 if sel = 1
//out = a0 if sel = 0
module mux2to1 #(parameter width = 16)
(
	input logic sel,
	input logic [width-1:0] a1,a0,
	output logic [width-1:0] out
);

always_comb
begin
	if (sel == 1'b0)
		out = a0;
	else
		out = a1;
end

endmodule : mux2to1