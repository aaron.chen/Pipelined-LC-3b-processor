import lc3b_types::*;

module mp3_tb;

timeunit 1ns;
timeprecision 1ns;

//clock
logic clk;

//*** PIPELINE DATAPATH -> MEMORY
logic read;
logic write;
lc3b_word address;
lc3b_c_line wdata;

//*** MEMORY -> PIPELINE DATAPATH
logic resp;
lc3b_c_line rdata;

/* Clock generator */
initial clk = 0;
always #5 clk = ~clk;

mp3 dut
(
	//clock
	.clk(clk),

	//CACHE L1 -> PHYS MEM
	.L1_pmem_read(read),
	.L1_pmem_write(write),
	.L1_pmem_address(address),
	.L1_pmem_wdata(wdata),

	//PHYS MEM -> CACHE L1
	/* Port A (PC stage)*/ 
	.L1_pmem_resp(resp),
	.L1_pmem_rdata(rdata)
);

//model for physical memory
physical_memory memory
(
	.clk(clk),

	.read(read),
	.write(write),
	.address(address),
	.wdata(wdata),
	.resp(resp),
	.rdata(rdata)
);

endmodule : mp3_tb
