import lc3b_types::*;

/*
 * out = SEXT[imm] (no left shift)
 */
module imm #(parameter width = 8)
(
	input [width-1:0] in,
	output lc3b_word out
);

assign out = $signed(in);

endmodule : imm