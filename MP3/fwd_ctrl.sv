import lc3b_types::*;

module fwd_ctrl
(
	input lc3b_reg ID_EX_rega,
	input lc3b_reg ID_EX_regb,
	input lc3b_reg EX_MEM_dr,
	input logic EX_MEM_load_regfile,
	input lc3b_reg MEM_WB_dr,
	input logic MEM_WB_load_regfile,
	
	output logic [1:0] fwd1mux_sel,
	output logic [1:0] fwd2mux_sel
);

always_comb
begin
// register A
if ((EX_MEM_load_regfile == 1'b1) && (EX_MEM_dr == ID_EX_rega))
begin
	// EX hazard
	fwd1mux_sel = 2'b10;
end
else if ((MEM_WB_load_regfile == 1'b1) && (MEM_WB_dr == ID_EX_rega))
begin
	// MEM hazard
	fwd1mux_sel = 2'b01;
end
else
begin
	fwd1mux_sel = 2'b00;
end

// register B
if ((EX_MEM_load_regfile == 1'b1) && (EX_MEM_dr == ID_EX_regb))
begin
	// EX hazard
	fwd2mux_sel = 2'b10;
end
else if ((MEM_WB_load_regfile == 1'b1) && (MEM_WB_dr == ID_EX_regb))
begin
	// MEM hazard
	fwd2mux_sel = 2'b01;
end
else
begin
	fwd2mux_sel = 2'b00;
end
end
endmodule : fwd_ctrl
