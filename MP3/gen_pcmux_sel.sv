import lc3b_types::*;

module gen_pcmux_sel
(
	input lc3b_opcode EX_MEM_opcode, MEM_WB_opcode,
	input logic cccomp,
	output logic [1:0] pcmux_sel
);

always_comb
begin
	if ((EX_MEM_opcode == op_br && cccomp == 1'b1) || (EX_MEM_opcode == op_jmp) || (EX_MEM_opcode == op_jsr))
	begin
		pcmux_sel = 2'b01;
	end
	else if (MEM_WB_opcode == op_trap)
	begin
		pcmux_sel = 2'b10;
	end
	else
	begin
		pcmux_sel = 2'b00;
	end
end

endmodule // gen_pcmux_sel