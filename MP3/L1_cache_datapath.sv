import lc3b_types::*;

module L1_cache_datapath
(
	input clk,

	//Cache -> CPU datapath
	output logic [lc3b_c_linewidth-1:0] mem_rdata,
	//CPU datapath -> Cache
	input logic [15:0] mem_address,
	input logic [lc3b_c_linewidth-1:0] mem_wdata,

	//Cache -> Main Memory or lower cache
	output logic [15:0] pmem_address,
	output logic [lc3b_c_linewidth-1:0] pmem_wdata,
	//Main Memory or lower cache -> Cache
	input logic [lc3b_c_linewidth-1:0] pmem_rdata,

	//Cache -> Cache Controller
	output logic valid_bit_arr1_out,
	output logic valid_bit_arr0_out,
	output logic dirty_bit_arr1_out,
	output logic dirty_bit_arr0_out,
	output logic lru_bit_arr_out,
	output logic tag1comp_out,
	output logic tag0comp_out,

	output logic proper_address,

	//Cache Controller -> Cache
	input logic increment_miss,
	input logic increment_hit,

	input logic cacheinmux_sel,
	input logic cacheoutmux_sel,

	input logic valid_bit_arr1_write,
	input logic valid_bit_arr0_write,
	input logic dirty_bit_arr1_in,
	input logic dirty_bit_arr0_in,
	input logic dirty_bit_arr1_write,
	input logic dirty_bit_arr0_write,
	input logic lru_bit_arr_in,
	input logic lru_bit_arr_write,
	input logic tag_arr1_write,
	input logic tag_arr0_write,
	input logic data_arr1_write,
	input logic data_arr0_write
);

//internal wires
lc3b_c_tag tag_arr1_out;
lc3b_c_tag tag_arr0_out;
lc3b_c_line data_arr1_out;
lc3b_c_line data_arr0_out;

lc3b_word cacheinwordmux_out;
lc3b_c_line wordreplacemux_out;
lc3b_c_line cacheinmux_out;
lc3b_c_line cacheoutmux_out;
lc3b_word cacheoutwordmux_out;

lc3b_word miss_counter_out;
lc3b_word hit_counter_out;

register #(.width(16)) miss_counter
(
	.clk,
	.load(increment_miss),
	.in(miss_counter_out+1'b1),
	.out(miss_counter_out)
);

register #(.width(16)) hit_counter
(
	.clk,
	.load(increment_hit),
	.in(hit_counter_out+1'b1),
	.out(hit_counter_out)
);

array #(.width(1)) valid_bit_arr1
(
	.clk,
	.write(valid_bit_arr1_write),
	.index(mem_address[6:4]),
	.datain(1'b1),
	.dataout(valid_bit_arr1_out)
);

array #(.width(1)) valid_bit_arr0
(
	.clk,
	.write(valid_bit_arr0_write),
	.index(mem_address[6:4]),
	.datain(1'b1),
	.dataout(valid_bit_arr0_out)
);

array #(.width(1)) dirty_bit_arr1
(
	.clk,
	.write(dirty_bit_arr1_write),
	.index(mem_address[6:4]),
	.datain(dirty_bit_arr1_in),
	.dataout(dirty_bit_arr1_out)
);

array #(.width(1)) dirty_bit_arr0
(
	.clk,
	.write(dirty_bit_arr0_write),
	.index(mem_address[6:4]),
	.datain(dirty_bit_arr0_in),
	.dataout(dirty_bit_arr0_out)
);

array #(.width(1)) lru_bit_arr
(
	.clk,
	.write(lru_bit_arr_write),
	.index(mem_address[6:4]),
	.datain(lru_bit_arr_in),
	.dataout(lru_bit_arr_out)
);

array #(.width(lc3b_c_taglen)) tag_arr1
(
	.clk,
	.write(tag_arr1_write),
	.index(mem_address[6:4]),
	.datain(mem_address[15:7]),
	.dataout(tag_arr1_out)
);

array #(.width(lc3b_c_taglen)) tag_arr0
(
	.clk,
	.write(tag_arr0_write),
	.index(mem_address[6:4]),
	.datain(mem_address[15:7]),
	.dataout(tag_arr0_out)
);

array #(.width(lc3b_c_linewidth)) data_arr1
(
	.clk,
	.write(data_arr1_write),
	.index(mem_address[6:4]),
	.datain(cacheinmux_out),
	.dataout(data_arr1_out)
);

array #(.width(lc3b_c_linewidth)) data_arr0
(
	.clk,
	.write(data_arr0_write),
	.index(mem_address[6:4]),
	.datain(cacheinmux_out),
	.dataout(data_arr0_out)
);

comparator #(.width(9)) tag1comp
(
	.a(tag_arr1_out),
	.b(mem_address[15:7]),
	.out(tag1comp_out)
);

comparator #(.width(9)) tag0comp
(
	.a(tag_arr0_out),
	.b(mem_address[15:7]),
	.out(tag0comp_out)
);

mux2to1 #(.width(128)) cacheinmux
(
	.sel(cacheinmux_sel),
	.a1(pmem_rdata),
	.a0(mem_wdata),
	.out(cacheinmux_out)
);

mux2to1 #(.width(128)) cacheoutmux
(
	.sel(cacheoutmux_sel),
	.a1(data_arr1_out),
	.a0(data_arr0_out),
	.out(cacheoutmux_out)
);


assign pmem_address = mem_address;
assign pmem_wdata = cacheoutmux_out;
assign proper_address = (mem_address != 16'hFFFE) && (mem_address  != 16'hFFFF);

always_comb
begin
	if (mem_address == 16'hFFFE)
	begin
		mem_rdata = hit_counter_out;
	end
	else if (mem_address == 16'hFFFF)
	begin
		mem_rdata = miss_counter_out;
	end
	else
	begin
		mem_rdata = cacheoutmux_out;
	end
end

endmodule : L1_cache_datapath