import lc3b_types::*;
//generates control word based on opcode and a few bits from the rest of the instruction
//NO COMPUTATION should be done here
module control_rom
(
	input lc3b_opcode opcode,
	input lc3b_shf_a shf_a, // also used as the imm sel
	input lc3b_shf_d shf_d,
	input lc3b_jsr_sel jsr_sel,

	output lc3b_control_word ctrl
);

//alias
logic imm_sel;
assign imm_sel = shf_a;

//signals that change based on opcode
always_comb
	begin
	/* Default assignments */
		//opcode stuff
		ctrl.opcode = opcode;
		ctrl.shf_a = shf_a;
		ctrl.shf_d = shf_d;
		ctrl.jsr_sel = jsr_sel;
		// decode
		ctrl.storemux_sel = 1'b0;
		ctrl.operand1mux_sel = 1'b0;
		ctrl.operand2mux_sel = 3'b00;
		// execute
		ctrl.aluop = alu_add;
		// memory access / branch?
		ctrl.marmux_sel = 1'b0;
		ctrl.mdrmux_sel = 2'b00;
		ctrl.read_b = 1'b0;
		ctrl.write_b = 1'b0;
		ctrl.wmask_b = 2'b11;
		// writeback
		ctrl.load_regfile = 1'b0;
		ctrl.load_cc = 1'b0;
		ctrl.datawritemux_sel = 2'b00;
		ctrl.drmux_sel = 1'b0;
		ctrl.inmux_sel = 1'b0;
		ctrl.load_regfilemux_sel = 1'b0;
		ctrl.wmaskmux_sel = 1'b0;

		case(opcode)
			op_add:
			begin
				ctrl.aluop = alu_add; //ADD
				ctrl.operand1mux_sel = 1'b0; //use SR1 as first operand
				ctrl.load_regfile = 1'b1; //load DR at end of operation
				ctrl.load_cc = 1'b1; //update nzp at end of operation
				ctrl.datawritemux_sel = 2'b00; //use aluout as data to load into DR
				if (imm_sel == 1'b0) //SR1 + SR2
				begin
					ctrl.operand2mux_sel = 3'b000; //SR2 as second operand
				end
				else
				begin
					ctrl.operand2mux_sel = 3'b001; //imm5 as second operand
				end
			end
			op_and:
			begin
				ctrl.aluop = alu_and; //AND
				ctrl.operand1mux_sel = 1'b0; //use SR1 as first operand
				ctrl.load_regfile = 1'b1; //load DR at end of operation
				ctrl.load_cc = 1'b1; //update nzp at end of operation
				ctrl.datawritemux_sel = 2'b00; //use aluout as data to load into DR
				if (imm_sel == 1'b0) //SR1 + SR2
				begin
					ctrl.operand2mux_sel = 3'b000; //SR2 as second operand
				end
				else
				begin
					ctrl.operand2mux_sel = 3'b001; //imm5 as second operand
				end
			end
			op_br:
			begin
				ctrl.operand1mux_sel = 1'b1; //Use PC as first operand
				ctrl.operand2mux_sel = 3'b100; //Use offset9 as second operand 
				ctrl.aluop = alu_add; //ADD

				// memory access / branch?
				ctrl.marmux_sel = 1'b0;
				ctrl.mdrmux_sel = 2'b00;
				ctrl.read_b = 1'b0;
				ctrl.write_b = 1'b0;
				ctrl.wmask_b = 2'b11;
				// writeback
				ctrl.load_regfile = 1'b0;
				ctrl.load_cc = 1'b0;
				ctrl.datawritemux_sel = 2'b00;
			end
			op_jmp:
			begin
				// Pass the value of SR1 through the ALU
				ctrl.operand1mux_sel = 1'b0; //Use SR1 as first operand
				ctrl.aluop = alu_pass; //PASS
				
				// NOTE: pcmux_sel is set to 1'b1 in the datapath
			end
			op_jsr:
			begin
				ctrl.drmux_sel = 1'b1;
				ctrl.inmux_sel = 1'b1;
				ctrl.load_regfilemux_sel = 1'b1; // Use the value from IF/ID instead of MEM/WB
				ctrl.load_regfile = 1'b1; // R7 = PC
				if (jsr_sel == 1'b0) begin // JSRR, use BaseR
					ctrl.aluop = alu_pass;
				end
				else begin // JSR, use PCoffset11
					ctrl.operand1mux_sel = 1'b1; // PC
					ctrl.operand2mux_sel = 3'b011; // (SEXT(PCoffset11) << 1)
					ctrl.aluop = alu_add;
				end
			end
			op_ldb:
			begin
				ctrl.operand1mux_sel = 1'b0; // use SR1 as first operand
				ctrl.operand2mux_sel = 3'b110; // use SEXT(offset6) without shift as second operand
				
				ctrl.aluop = alu_add; // ADD
				ctrl.marmux_sel = 1'b0; // use alu out as the address to load into the mar
				
				ctrl.read_b = 1'b1; // read from memory
//				ctrl.mdrmux_sel = 2'b10; // use the data read from memory as the data to load into MDR
				
				ctrl.datawritemux_sel = 2'b10; // write back the data from memory
				ctrl.load_regfile = 1'b1; // load data into regfile
				ctrl.load_cc = 1'b1; // set cc
			end
			op_ldi:
			begin
				ctrl.operand1mux_sel = 1'b0; // use SR1 as first operand
				ctrl.operand2mux_sel = 3'b101; // use off6SHIFTSEXT as second operand
				
				ctrl.aluop = alu_add;
				ctrl.marmux_sel = 1'b0; // use alu out and then mem out as the addresses to load into the mar
				
				ctrl.read_b = 1'b1; // read from memory
				
				ctrl.datawritemux_sel = 2'b01; // write back the data from memory
				ctrl.load_regfile = 1'b1;
				ctrl.load_cc = 1'b1; // set cc
			end
			op_ldr:
			begin
				ctrl.operand1mux_sel = 1'b0; //use SR1 as first operand
				ctrl.operand2mux_sel = 3'b101; //use offset6 as second operand

				ctrl.aluop = alu_add; //ADD
				ctrl.marmux_sel = 1'b0; //Use alu out as the address to load into the mar

				ctrl.read_b = 1'b1; //read from memory
				ctrl.mdrmux_sel = 2'b10; // Use the data read from memory as the data to load into MDR

				ctrl.datawritemux_sel = 2'b01; //write back the data from memory
				ctrl.load_regfile = 1'b1; //load MDR into regfile
				ctrl.load_cc = 1'b1; //set cc
			end
			op_lea:
			begin
				ctrl.operand1mux_sel = 1'b1; // PC
				ctrl.operand2mux_sel = 3'b100; // SEXT(PCoffset9) << 1

				ctrl.aluop = alu_add; // ADD

				ctrl.datawritemux_sel = 2'b00; // use aluout as data to load into DR
				ctrl.drmux_sel = 1'b0;
				ctrl.inmux_sel = 1'b0;
				ctrl.load_regfile = 1'b1;
				ctrl.load_cc = 1'b1; // set cc
			end
			op_not:
			begin
				ctrl.aluop = alu_not; //NOT
				ctrl.operand1mux_sel = 1'b0; //use SR1 as first operand
				ctrl.load_regfile = 1'b1; //load DR at end of operation
				ctrl.load_cc = 1'b1; //update nzp at end of operation
				ctrl.datawritemux_sel = 2'b00; //use aluout as data to load into DR
			end
			op_rti:
			begin
				ctrl = 0; /* Unknown opcode, set control word to zero */
			end
			op_shf:
			begin
				ctrl.operand1mux_sel = 1'b0; // use SR1 as first operand
				ctrl.operand2mux_sel = 3'b111; // use ZEXT(imm4) as second operand
				if (shf_d == 0) begin
					ctrl.aluop = alu_sll;
				end
				else begin
					if (shf_a == 0) begin
						ctrl.aluop = alu_srl;
					end
					else begin
						ctrl.aluop = alu_sra;
					end
				end

				ctrl.datawritemux_sel = 2'b00; // use aluout as data to load into DR
				ctrl.load_regfile = 1'b1; // load DR at end of operation
				ctrl.load_cc = 1'b1; // set cc
			end
			op_stb:
			begin
				ctrl.storemux_sel = 1'b1;
				ctrl.operand1mux_sel = 1'b0; // use SR1 as first operand
				ctrl.operand2mux_sel = 3'b110; // use SEXT(offset6) without shift as second operand

				ctrl.aluop = alu_add; // ADD

				ctrl.marmux_sel = 1'b0; // use alu out as the address to load into the mar
				ctrl.mdrmux_sel = 2'b11; // use {8'b00000000,SR[7:0]} as data to load into the mdr
				ctrl.wmaskmux_sel = 1'b1; // use stb_wmask as wmask_b
				ctrl.write_b = 1'b1; // store into memory
			end
			op_sti:
			begin
//				ctrl = 0; /* Unknown opcode, set control word to zero */
				ctrl.storemux_sel = 1'b1; // get SR value as regfile "sr2_out" even though we're not using it in the ALU (will be placed in MDR)
				ctrl.operand1mux_sel = 1'b0; // use SR1 as first operand
				ctrl.operand2mux_sel = 3'b101; // use offset6SHIFTSEXT as second operand
				ctrl.aluop = alu_add;
				
				ctrl.marmux_sel = 1'b0; // use alu out and then mem out as the addresses to load into the mar
								
				ctrl.read_b = 1'b1; // read address from memory
			end
			op_str:
			begin
				ctrl.storemux_sel = 1'b1; //get SR value as regfile "sr2_out" even though we're not using it in the ALU (will be placed in MDR)
				ctrl.operand1mux_sel = 1'b0; //use SR1 as first operand
				ctrl.operand2mux_sel = 3'b101; //use offset6 as second operand
				ctrl.aluop = alu_add; //ADD
				
				ctrl.marmux_sel = 1'b0; //Use alu out as the address to load into the mar

				ctrl.mdrmux_sel = 2'b01; // Use SR as the data to load into the mdr
				
				ctrl.write_b = 1'b1; //store into memory
			end
			op_trap:
			begin
				/* load R7 with incremented PC */
				ctrl.drmux_sel = 1'b1;
				ctrl.inmux_sel = 1'b1;
				ctrl.load_regfilemux_sel = 1'b1; // Use the value from IF/ID instead of MEM/WB
				ctrl.load_regfile = 1'b1; // R7 = PC
				
				/* load PC with memWord[ZEXT(trapvect8) << 1] */
				ctrl.marmux_sel = 1'b1;
				ctrl.datawritemux_sel = 2'b01;
				// pcmux_sel is set in the datapath with the gen_pcmux_sel module
			end
			default:
			begin
				ctrl = 0; /* Unknown opcode, set control word to zero */
			end
		endcase
	end

endmodule : control_rom