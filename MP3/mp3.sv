import lc3b_types::*;

// top level
module mp3
(
	//clock
	input logic clk,

	//CACHE L1 -> PHYS MEM
	output logic L1_pmem_read,
	output logic L1_pmem_write,
	output lc3b_word L1_pmem_address,
	output lc3b_c_line L1_pmem_wdata,

	//PHYS MEM -> CACHE L1
	/* Port A (PC stage)*/ 
	input logic L1_pmem_resp,
	input lc3b_c_line L1_pmem_rdata
);

//L2 arbiter -> L1 cache
logic L2_pmem_read;
logic L2_pmem_write;
lc3b_word L2_pmem_address;
lc3b_c_line L2_pmem_wdata;

//CACHE L1 -> L2 Arbiter / Cache
/* Port A (PC stage)*/ 
logic L2_pmem_resp;
lc3b_c_line L2_pmem_rdata;

//Cache -> Cache Controller
logic L1_valid_bit_arr1_out;
logic L1_valid_bit_arr0_out;
logic L1_dirty_bit_arr1_out;
logic L1_dirty_bit_arr0_out;
logic L1_lru_bit_arr_out;
logic L1_tag1comp_out;
logic L1_tag0comp_out;

logic L1_increment_miss;
logic L1_increment_hit;

//Cache Controller -> Cache
logic L1_cacheinmux_sel;
logic L1_cacheoutmux_sel;
logic L1_proper_address;

logic L1_valid_bit_arr1_write;
logic L1_valid_bit_arr0_write;
logic L1_dirty_bit_arr1_in;
logic L1_dirty_bit_arr0_in;
logic L1_dirty_bit_arr1_write;
logic L1_dirty_bit_arr0_write;
logic L1_lru_bit_arr_in;
logic L1_lru_bit_arr_write;
logic L1_tag_arr1_write;
logic L1_tag_arr0_write;
logic L1_data_arr1_write;
logic L1_data_arr0_write;

//L2 Cache <-> L2 arbiter
lc3b_word I_pmem_address;
lc3b_c_line I_pmem_rdata;

lc3b_word D_pmem_address;
lc3b_c_line D_pmem_wdata;
lc3b_c_line D_pmem_rdata;

logic I_pmem_read;
logic I_pmem_resp;

logic D_pmem_read;
logic D_pmem_write;
logic D_pmem_resp;

logic L2_addressmux_sel;
logic D_cache_MDR_mux_sel;

logic I_mar_load;
logic I_mdr_load;

logic D_mar_load;
logic D_mdrmux_sel;
logic D_mdr_load;

//L2 Cache datapath <-> L2 cache controller

//Cache -> Cache Controller
logic I_valid_bit_arr1_out;
logic I_valid_bit_arr0_out;
logic I_dirty_bit_arr1_out;
logic I_dirty_bit_arr0_out;
logic I_lru_bit_arr_out;
logic I_tag1comp_out;
logic I_tag0comp_out;

//Cache Controller -> Cache
logic I_cacheinmux_sel;
logic I_cacheoutmux_sel;

logic I_increment_miss;
logic I_increment_hit;
logic I_proper_address;

logic I_valid_bit_arr1_write;
logic I_valid_bit_arr0_write;
logic I_dirty_bit_arr1_in;
logic I_dirty_bit_arr0_in;
logic I_dirty_bit_arr1_write;
logic I_dirty_bit_arr0_write;
logic I_lru_bit_arr_in;
logic I_lru_bit_arr_write;
logic I_tag_arr1_write;
logic I_tag_arr0_write;
logic I_data_arr1_write;
logic I_data_arr0_write;

//Cache -> Cache Controller
logic D_valid_bit_arr1_out;
logic D_valid_bit_arr0_out;
logic D_dirty_bit_arr1_out;
logic D_dirty_bit_arr0_out;
logic D_lru_bit_arr_out;
logic D_tag1comp_out;
logic D_tag0comp_out;

//Cache Controller -> Cache
logic D_increment_miss;
logic D_increment_hit;
logic D_proper_address;

logic D_cacheinmux_sel;
logic D_cacheoutmux_sel;

logic D_valid_bit_arr1_write;
logic D_valid_bit_arr0_write;
logic D_dirty_bit_arr1_in;
logic D_dirty_bit_arr0_in;
logic D_dirty_bit_arr1_write;
logic D_dirty_bit_arr0_write;
logic D_lru_bit_arr_in;
logic D_lru_bit_arr_write;
logic D_tag_arr1_write;
logic D_tag_arr0_write;
logic D_data_arr1_write;
logic D_data_arr0_write;

//pipeline

/* Port A (PC stage)*/
logic read_a;
logic write_a;
logic [1:0] wmask_a;
logic [15:0] address_a;
logic [15:0] wdata_a;
logic resp_a;
logic [15:0] rdata_a;

/* Port B (MEM stage)*/
logic read_b;
logic write_b;
logic [1:0] wmask_b;
logic [15:0] address_b;
logic [15:0] wdata_b;
logic resp_b;
logic [15:0] rdata_b;

//data writeback
logic D_WB_pmem_resp;
logic D_WB_pmem_write;
lc3b_c_line D_WB_pmem_wdata;

L1_cache_datapath L1_cache_datapath
(
	.clk(clk),

	//Cache -> CPU datapath
	.mem_rdata(L2_pmem_rdata),
	//CPU datapath -> Cache
	.mem_address(L2_pmem_address),
	.mem_wdata(L2_pmem_wdata),

	//Cache -> Main Memory or lower cache
	.pmem_address(L1_pmem_address),
	.pmem_wdata(L1_pmem_wdata),
	//Main Memory or lower cache -> Cache
	.pmem_rdata(L1_pmem_rdata),

	//Cache -> Cache Controller
	.valid_bit_arr1_out(L1_valid_bit_arr1_out),
	.valid_bit_arr0_out(L1_valid_bit_arr0_out),
	.dirty_bit_arr1_out(L1_dirty_bit_arr1_out),
	.dirty_bit_arr0_out(L1_dirty_bit_arr0_out),
	.lru_bit_arr_out(L1_lru_bit_arr_out),
	.tag1comp_out(L1_tag1comp_out),
	.tag0comp_out(L1_tag0comp_out),

	.proper_address(L1_proper_address),

	.increment_miss(L1_increment_miss),
	.increment_hit(L1_increment_hit),

	//Cache Controller -> Cache
	.cacheinmux_sel(L1_cacheinmux_sel),
	.cacheoutmux_sel(L1_cacheoutmux_sel),

	.valid_bit_arr1_write(L1_valid_bit_arr1_write),
	.valid_bit_arr0_write(L1_valid_bit_arr0_write),
	.dirty_bit_arr1_in(L1_dirty_bit_arr1_in),
	.dirty_bit_arr0_in(L1_dirty_bit_arr0_in),
	.dirty_bit_arr1_write(L1_dirty_bit_arr1_write),
	.dirty_bit_arr0_write(L1_dirty_bit_arr0_write),
	.lru_bit_arr_in(L1_lru_bit_arr_in),
	.lru_bit_arr_write(L1_lru_bit_arr_write),
	.tag_arr1_write(L1_tag_arr1_write),
	.tag_arr0_write(L1_tag_arr0_write),
	.data_arr1_write(L1_data_arr1_write),
	.data_arr0_write(L1_data_arr0_write)
);

L1_cache_controller L1_cache_controller
(
	.clk(clk),

	//Cache Controller -> CPU datapath
	.mem_resp(L2_pmem_resp),
	//CPU datapath -> Cache Controller
	.mem_read(L2_pmem_read),
	.mem_write(L2_pmem_write),

	//Cache Controller -> Main Memory or lower cache
	.pmem_read(L1_pmem_read),
	.pmem_write(L1_pmem_write),
	//Main Memory or lower cache -> Cache Controller
	.pmem_resp(L1_pmem_resp),

	//Cache -> Cache Controller
	.valid_bit_arr1_out(L1_valid_bit_arr1_out),
	.valid_bit_arr0_out(L1_valid_bit_arr0_out),
	.dirty_bit_arr1_out(L1_dirty_bit_arr1_out),
	.dirty_bit_arr0_out(L1_dirty_bit_arr0_out),
	.lru_bit_arr_out(L1_lru_bit_arr_out),
	.tag1comp_out(L1_tag1comp_out),
	.tag0comp_out(L1_tag0comp_out),

	.increment_miss(L1_increment_miss),
	.increment_hit(L1_increment_hit),

	.proper_address(L1_proper_address),

	//Cache Controller -> Cache
	.cacheinmux_sel(L1_cacheinmux_sel),
	.cacheoutmux_sel(L1_cacheoutmux_sel),

	.valid_bit_arr1_write(L1_valid_bit_arr1_write),
	.valid_bit_arr0_write(L1_valid_bit_arr0_write),
	.dirty_bit_arr1_in(L1_dirty_bit_arr1_in),
	.dirty_bit_arr0_in(L1_dirty_bit_arr0_in),
	.dirty_bit_arr1_write(L1_dirty_bit_arr1_write),
	.dirty_bit_arr0_write(L1_dirty_bit_arr0_write),
	.lru_bit_arr_in(L1_lru_bit_arr_in),
	.lru_bit_arr_write(L1_lru_bit_arr_write),
	.tag_arr1_write(L1_tag_arr1_write),
	.tag_arr0_write(L1_tag_arr0_write),
	.data_arr1_write(L1_data_arr1_write),
	.data_arr0_write(L1_data_arr0_write)
);

arbiter_datapath arbiter_data
(
	.clk(clk),

	.I_pmem_address(I_pmem_address),
	.I_pmem_rdata(I_pmem_rdata),

	.D_pmem_address(D_pmem_address),
	.D_pmem_wdata(D_WB_pmem_wdata),
	.D_pmem_rdata(D_pmem_rdata),

	.L2_pmem_rdata(L2_pmem_rdata),
	.L2_pmem_wdata(L2_pmem_wdata),
	.L2_pmem_address(L2_pmem_address),

	.L2_addressmux_sel(L2_addressmux_sel),
	.D_cache_MDR_mux_sel(D_cache_MDR_mux_sel),

	.I_mar_load(I_mar_load),
	.I_mdr_load(I_mdr_load),

	.D_mar_load(D_mar_load),
	.D_mdr_load(D_mdr_load)
);

arbiter_controller arbiter_ctrl
(
	.clk(clk),

	.I_pmem_read(I_pmem_read),
	.I_pmem_resp(I_pmem_resp),

	.D_pmem_read(D_pmem_read),
	.D_pmem_write(D_WB_pmem_write),
	.D_pmem_resp(D_WB_pmem_resp),

	.L2_pmem_read(L2_pmem_read),
	.L2_pmem_write(L2_pmem_write),
	.L2_pmem_resp(L2_pmem_resp),

	.L2_addressmux_sel(L2_addressmux_sel),
	.D_cache_MDR_mux_sel(D_cache_MDR_mux_sel),

	.I_mar_load(I_mar_load),
	.I_mdr_load(I_mdr_load),

	.D_mar_load(D_mar_load),
	.D_mdr_load(D_mdr_load)
);

L2_cache_datapath I_cache_datapath
(
	.clk(clk),

	//Cache -> CPU datapath
	.mem_rdata(rdata_a),
	//CPU datapath -> Cache
	.mem_address(address_a),
	.mem_wdata(wdata_a),
	.mem_byte_enable(wmask_a),

	//Cache -> Main Memory or lower cache
	.pmem_address(I_pmem_address),
	.pmem_wdata(),
	//Main Memory or lower cache -> Cache
	.pmem_rdata(I_pmem_rdata),

	//Cache -> Cache Controller
	.valid_bit_arr1_out(I_valid_bit_arr1_out),
	.valid_bit_arr0_out(I_valid_bit_arr0_out),
	.dirty_bit_arr1_out(I_dirty_bit_arr1_out),
	.dirty_bit_arr0_out(I_dirty_bit_arr0_out),
	.lru_bit_arr_out(I_lru_bit_arr_out),
	.tag1comp_out(I_tag1comp_out),
	.tag0comp_out(I_tag0comp_out),

	.increment_miss(I_increment_miss),
	.increment_hit(I_increment_hit),

	.proper_address(I_proper_address),

	//Cache Controller -> Cache
	.cacheinmux_sel(I_cacheinmux_sel),
	.cacheoutmux_sel(I_cacheoutmux_sel),

	.valid_bit_arr1_write(I_valid_bit_arr1_write),
	.valid_bit_arr0_write(I_valid_bit_arr0_write),
	.dirty_bit_arr1_in(I_dirty_bit_arr1_in),
	.dirty_bit_arr0_in(I_dirty_bit_arr0_in),
	.dirty_bit_arr1_write(I_dirty_bit_arr1_write),
	.dirty_bit_arr0_write(I_dirty_bit_arr0_write),
	.lru_bit_arr_in(I_lru_bit_arr_in),
	.lru_bit_arr_write(I_lru_bit_arr_write),
	.tag_arr1_write(I_tag_arr1_write),
	.tag_arr0_write(I_tag_arr0_write),
	.data_arr1_write(I_data_arr1_write),
	.data_arr0_write(I_data_arr0_write)
);

L2_cache_controller I_cache_controller
(
	.clk,

	//Cache Controller -> CPU datapath
	.mem_resp(resp_a),
	//CPU datapath -> Cache Controller
	.mem_read(read_a),
	.mem_write(write_a),

	//Cache Controller -> Main Memory or lower cache
	.pmem_read(I_pmem_read),
	.pmem_write(I_pmem_write),
	//Main Memory or lower cache -> Cache Controller
	.pmem_resp(I_pmem_resp),

	//Cache -> Cache Controller
	.valid_bit_arr1_out(I_valid_bit_arr1_out),
	.valid_bit_arr0_out(I_valid_bit_arr0_out),
	.dirty_bit_arr1_out(I_dirty_bit_arr1_out),
	.dirty_bit_arr0_out(I_dirty_bit_arr0_out),
	.lru_bit_arr_out(I_lru_bit_arr_out),
	.tag1comp_out(I_tag1comp_out),
	.tag0comp_out(I_tag0comp_out),

	.increment_miss(I_increment_miss),
	.increment_hit(I_increment_hit),

	.proper_address(I_proper_address),

	//Cache Controller -> Cache
	.cacheinmux_sel(I_cacheinmux_sel),
	.cacheoutmux_sel(I_cacheoutmux_sel),

	.valid_bit_arr1_write(I_valid_bit_arr1_write),
	.valid_bit_arr0_write(I_valid_bit_arr0_write),
	.dirty_bit_arr1_in(I_dirty_bit_arr1_in),
	.dirty_bit_arr0_in(I_dirty_bit_arr0_in),
	.dirty_bit_arr1_write(I_dirty_bit_arr1_write),
	.dirty_bit_arr0_write(I_dirty_bit_arr0_write),
	.lru_bit_arr_in(I_lru_bit_arr_in),
	.lru_bit_arr_write(I_lru_bit_arr_write),
	.tag_arr1_write(I_tag_arr1_write),
	.tag_arr0_write(I_tag_arr0_write),
	.data_arr1_write(I_data_arr1_write),
	.data_arr0_write(I_data_arr0_write)
);

writebackbuffer D_writebackbuffer
(
	.clk,    // Clock
	.pmem_write(D_pmem_write), // Clock Enable
	.pmem_wdata(D_pmem_wdata),

	.pmem_resp(D_pmem_resp),  // Asynchronous reset active low

	.L2_pmem_resp(D_WB_pmem_resp),
	.L2_pmem_write(D_WB_pmem_write),
	.L2_pmem_wdata(D_WB_pmem_wdata)
);

L2_cache_datapath D_cache_datapath
(
	.clk(clk),

	//Cache -> CPU datapath
	.mem_rdata(rdata_b),
	//CPU datapath -> Cache
	.mem_address(address_b),
	.mem_wdata(wdata_b),
	.mem_byte_enable(wmask_b),

	//Cache -> Main Memory or lower cache
	.pmem_address(D_pmem_address),
	.pmem_wdata(D_pmem_wdata),
	//Main Memory or lower cache -> Cache
	.pmem_rdata(D_pmem_rdata),

	//Cache -> Cache Controller
	.valid_bit_arr1_out(D_valid_bit_arr1_out),
	.valid_bit_arr0_out(D_valid_bit_arr0_out),
	.dirty_bit_arr1_out(D_dirty_bit_arr1_out),
	.dirty_bit_arr0_out(D_dirty_bit_arr0_out),
	.lru_bit_arr_out(D_lru_bit_arr_out),
	.tag1comp_out(D_tag1comp_out),
	.tag0comp_out(D_tag0comp_out),

	.proper_address(D_proper_address),

	.increment_miss(D_increment_miss),
	.increment_hit(D_increment_hit),

	//Cache Controller -> Cache
	.cacheinmux_sel(D_cacheinmux_sel),
	.cacheoutmux_sel(D_cacheoutmux_sel),

	.valid_bit_arr1_write(D_valid_bit_arr1_write),
	.valid_bit_arr0_write(D_valid_bit_arr0_write),
	.dirty_bit_arr1_in(D_dirty_bit_arr1_in),
	.dirty_bit_arr0_in(D_dirty_bit_arr0_in),
	.dirty_bit_arr1_write(D_dirty_bit_arr1_write),
	.dirty_bit_arr0_write(D_dirty_bit_arr0_write),
	.lru_bit_arr_in(D_lru_bit_arr_in),
	.lru_bit_arr_write(D_lru_bit_arr_write),
	.tag_arr1_write(D_tag_arr1_write),
	.tag_arr0_write(D_tag_arr0_write),
	.data_arr1_write(D_data_arr1_write),
	.data_arr0_write(D_data_arr0_write)
);

L2_cache_controller D_cache_controller
(
	.clk,

	//Cache Controller -> CPU datapath
	.mem_resp(resp_b),
	//CPU datapath -> Cache Controller
	.mem_read(read_b),
	.mem_write(write_b),

	//Cache Controller -> Main Memory or lower cache
	.pmem_read(D_pmem_read),
	.pmem_write(D_pmem_write),
	//Main Memory or lower cache -> Cache Controller
	.pmem_resp(D_WB_pmem_resp || D_pmem_resp),

	//Cache -> Cache Controller
	.valid_bit_arr1_out(D_valid_bit_arr1_out),
	.valid_bit_arr0_out(D_valid_bit_arr0_out),
	.dirty_bit_arr1_out(D_dirty_bit_arr1_out),
	.dirty_bit_arr0_out(D_dirty_bit_arr0_out),
	.lru_bit_arr_out(D_lru_bit_arr_out),
	.tag1comp_out(D_tag1comp_out),
	.tag0comp_out(D_tag0comp_out),

	.proper_address(D_proper_address),

	.increment_miss(D_increment_miss),
	.increment_hit(D_increment_hit),

	//Cache Controller -> Cache
	.cacheinmux_sel(D_cacheinmux_sel),
	.cacheoutmux_sel(D_cacheoutmux_sel),

	.valid_bit_arr1_write(D_valid_bit_arr1_write),
	.valid_bit_arr0_write(D_valid_bit_arr0_write),
	.dirty_bit_arr1_in(D_dirty_bit_arr1_in),
	.dirty_bit_arr0_in(D_dirty_bit_arr0_in),
	.dirty_bit_arr1_write(D_dirty_bit_arr1_write),
	.dirty_bit_arr0_write(D_dirty_bit_arr0_write),
	.lru_bit_arr_in(D_lru_bit_arr_in),
	.lru_bit_arr_write(D_lru_bit_arr_write),
	.tag_arr1_write(D_tag_arr1_write),
	.tag_arr0_write(D_tag_arr0_write),
	.data_arr1_write(D_data_arr1_write),
	.data_arr0_write(D_data_arr0_write)
);

//pipeline datapath
datapath datapath
(
	.clk(clk),

	/* Port A (PC stage)*/
	.read_a(read_a),
	.write_a(write_a),
	.wmask_a(wmask_a),
	.address_a(address_a),
	.wdata_a(wdata_a),
	.resp_a(resp_a),
	.rdata_a(rdata_a),

	/* Port B (MEM stage)*/
	.read_b(read_b),
	.write_b(write_b),
	.wmask_b(wmask_b),
	.address_b(address_b),
	.wdata_b(wdata_b),
	.resp_b(resp_b),
	.rdata_b(rdata_b)
);


endmodule : mp3
