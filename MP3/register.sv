//on every clk cycle, if load = 1, then make internal data = in
//output data at out
module register #(parameter width = 16)
(
	input clk,
	input load,
	input [width-1:0] in,
	output logic [width-1:0] out
);

logic [width-1:0] data;

/* Altera device registers are 0 at power on. Specify this
 * so that Modelsim works as expected.
 */
initial
begin
	data = 1'b0;
end

always @(posedge clk)
begin
	if (load)
	begin
		data = in;
	end
end

always_comb
begin
	out = data;
end

endmodule : register
