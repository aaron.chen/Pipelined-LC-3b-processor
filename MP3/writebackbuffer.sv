import lc3b_types::*;

module writebackbuffer
(
	input clk,    // Clock
	input logic pmem_write, // Clock Enable
	input lc3b_c_line pmem_wdata,

	output logic pmem_resp,  // Asynchronous reset active low

	input logic L2_pmem_resp,
	output logic L2_pmem_write,
	output lc3b_c_line L2_pmem_wdata
);

logic buffer_load;

enum int unsigned
{
	free,
	buffer_writing,
	pmem_writing,
	done_writing
} state, next_state;

register #(.width(128)) buffer
(
	.clk,
	.load(buffer_load),
	.in(pmem_wdata),
	.out(L2_pmem_wdata)
);

always_comb
begin
	buffer_load = 1'b0;
	pmem_resp = 1'b0;
	L2_pmem_write = 1'b0;

	case (state)
		buffer_writing:
		begin
			buffer_load = 1'b1;
		end
		pmem_writing:
		begin
			L2_pmem_write = 1'b1;
		end
		done_writing:
		begin
			pmem_resp = 1'b1;
		end
		free:
		begin
		end
	endcase
end

always_comb
begin
	next_state = state;
	case (state)
		buffer_writing:
		begin
			next_state = pmem_writing;
		end
		pmem_writing:
		begin
			if (L2_pmem_resp == 1'b1)
			begin
				next_state = done_writing;
			end
			else
			begin
				next_state = state;
			end
		end
		done_writing:
		begin
			if (pmem_write == 1'b0)
			begin
				next_state = free;
			end
		end
		free:
		begin
			if (pmem_write == 1'b1)
			begin
				next_state = buffer_writing;
			end
		end
	endcase
end

always_ff @(posedge clk)
begin
	state <= next_state;
end

endmodule