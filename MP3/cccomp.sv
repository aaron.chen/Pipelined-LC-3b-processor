import lc3b_types::*;

//compare nzp_query (from instruction) to current nzp_status (from nzp register)
//if one pair of bits both = 1, then produce 1, else produce 0s

module cccomp
(
	input lc3b_nzp nzp_query, nzp_status,
	output logic to_branch
);

always_comb
begin
	if ((nzp_query[0] == 1) && (nzp_status[0] == 1))
		to_branch = 1;
	else if ((nzp_query[1] == 1) && (nzp_status[1] == 1))
		to_branch = 1;
	else if ((nzp_query[2] == 1) && (nzp_status[2] == 1))
		to_branch = 1;
	else
		to_branch = 0;
end

endmodule : cccomp