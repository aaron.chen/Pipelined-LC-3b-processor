import lc3b_types::*;
//specialized register that holds fields needed for the EXECUTE stage from the DECODE stage
//also has special fields to indicate data fields
//on every clock cycle, if load == 1, internal data fields are assigned to the respective in fields
//output various data fields
module ID_EX_register
(
	input logic clk,
	input logic load,
	input logic squash,
	input lc3b_control_word ctrl_in,

	input lc3b_word operand1_in, 
	input lc3b_word operand2_in, 
	input lc3b_word regfile_sr_in, // for use in str
	input lc3b_reg dr_nzp_in, //for use in ldr or in branch comp
	input lc3b_trap8 trap8_in,
	input lc3b_reg rega_in,
	input lc3b_reg regb_in,

	output lc3b_control_word ctrl_out,
	output lc3b_word operand1_out, 
	output lc3b_word operand2_out, 
	output lc3b_word regfile_sr_out, // for use in str
	output lc3b_reg dr_nzp_out, //for use in ldr or in branch comp
	output lc3b_trap8 trap8_out,
	output lc3b_reg rega_out,
	output lc3b_reg regb_out
);

lc3b_word operand1_data; 
lc3b_word operand2_data; 
lc3b_word regfile_sr_data; // for use in str
lc3b_reg dr_nzp_data; //for use in ldr or in branch comp
lc3b_trap8 trap8_data;
lc3b_control_word ctrl_data;
lc3b_reg rega_data;
lc3b_reg regb_data;

/* Altera device registers are 0 at power on. Specify this
 * so that Modelsim works as expected.
 */
initial
begin
	operand1_data = 1'b0;
	operand2_data = 1'b0; 
	regfile_sr_data = 1'b0;
	dr_nzp_data = 1'b0; 
	trap8_data = 1'b0;
	ctrl_data = 1'b0;
	rega_data = 1'b0;
	regb_data = 1'b0;
end

always @(posedge clk)
begin
	if (squash)
	begin
		operand1_data = 0;
		operand2_data = 0;
		regfile_sr_data = 0;
		dr_nzp_data = 0;
		trap8_data = 0;
		ctrl_data = 0;
		rega_data = 0;
		regb_data = 0;
	end
	else if (load)
	begin
		operand1_data = operand1_in;
		operand2_data = operand2_in;
		regfile_sr_data = regfile_sr_in;
		dr_nzp_data = dr_nzp_in;
		trap8_data = trap8_in;
		ctrl_data = ctrl_in;
		rega_data = rega_in;
		regb_data = regb_in;
	end
end

always_comb
begin
	operand1_out = operand1_data;
	operand2_out = operand2_data;
	regfile_sr_out = regfile_sr_data;
	dr_nzp_out = dr_nzp_data;
	trap8_out = trap8_data;
	ctrl_out = ctrl_data;
	rega_out = rega_data;
	regb_out = regb_data;
end

endmodule : ID_EX_register
