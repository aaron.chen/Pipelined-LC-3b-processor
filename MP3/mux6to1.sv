//out = a5 if sel = 5,6,7
//out = a4 if sel = 4
//out = a3 if sel = 3
//out = a2 if sel = 2
//out = a1 if sel = 1
//out = a0 if sel = 0
module mux6to1 #(parameter width = 16)
(
	input logic [2:0] sel,
	input logic [width-1:0] a5, a4, a3, a2, a1, a0,
	output logic [width-1:0] out
);

always_comb
begin
	if (sel == 3'b000)
		out = a0;
	else if (sel == 3'b001)
		out = a1;
	else if (sel == 3'b010)
		out = a2;
	else if (sel == 3'b011)
		out = a3;
	else if (sel == 3'b100)
		out = a4;
	else
		out = a5;
end

endmodule : mux6to1