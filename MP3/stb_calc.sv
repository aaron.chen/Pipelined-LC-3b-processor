import lc3b_types::*;

module stb_calc
(
	input lc3b_word in,
	input logic addr_lsb,
	output lc3b_word stb_data,
	output lc3b_mem_wmask stb_wmask
);

always_comb
begin
	if (addr_lsb == 0) begin
		stb_data = in;
		stb_wmask = 2'b01;
	end
	else begin
		stb_data = {in[7:0],8'b00000000};
		stb_wmask = 2'b10;
	end
end

endmodule : stb_calc