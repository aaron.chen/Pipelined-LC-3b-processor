//out = a4 if sel = 4,5,6,7
//out = a3 if sel = 3
//out = a2 if sel = 2
//out = a1 if sel = 1
//out = a0 if sel = 0
module mux5to1 #(parameter width = 16)
(
	input logic [2:0] sel,
	input logic [width-1:0] a4, a3, a2, a1, a0,
	output logic [width-1:0] out
);

always_comb
begin
	if (sel == 3'b000)
		out = a0;
	else if (sel == 3'b001)
		out = a1;
	else if (sel == 3'b010)
		out = a2;
	else if (sel == 3'b011)
		out = a3;
	else
		out = a4;
end

endmodule : mux5to1