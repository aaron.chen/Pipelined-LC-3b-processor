module arbiter_controller
(
	input clk,

	input logic I_pmem_read,
	output logic I_pmem_resp,

	input logic D_pmem_read,
	input logic D_pmem_write,
	output logic D_pmem_resp,

	output logic L2_pmem_read,
	output logic L2_pmem_write,
	input logic L2_pmem_resp,

	output logic L2_addressmux_sel,
	output logic D_cache_MDR_mux_sel,

	output logic I_mar_load,
	output logic I_mdr_load,

	output logic D_mar_load,
	output logic D_mdr_load
	
);

enum int unsigned
{
	S_D_last,
	S_I_last,
	S_I_read1,
	S_I_read2,
	S_I_read3,
	S_D_read1,
	S_D_read2,
	S_D_read3,
	S_D_write1,
	S_D_write2,
	S_D_write3
} state, next_state;

//define state actions for signals
always_comb
begin : state_actions
	I_pmem_resp = 1'b0;
	D_pmem_resp = 1'b0;

	L2_pmem_read = 1'b0;
	L2_pmem_write = 1'b0;

	L2_addressmux_sel = 1'b0;
	D_cache_MDR_mux_sel = 1'b0;

	I_mar_load = 1'b0;
	I_mdr_load = 1'b0;
	D_mar_load = 1'b0;
	D_mdr_load = 1'b0;

	case(state)
		S_I_read1:
		begin
			//load mar with I addr
			I_mar_load = 1'b1;
		end
		S_I_read2:
		begin
			//read using I_MAR address
			L2_addressmux_sel = 1'b1;
			//read
			//load mdr with read pmem
			I_mdr_load = 1'b1;
			L2_pmem_read = 1'b1;
		end
		S_I_read3:
		begin
			//signal success
			I_pmem_resp = 1'b1;
		end
		S_I_last:
		begin
		end
		S_D_read1:
		begin
			//load mar with D addr
			D_mar_load = 1'b1;
		end
		S_D_read2:
		begin
			D_cache_MDR_mux_sel = 1'b0;
			//read using D_MAR address
			L2_addressmux_sel = 1'b0;
			//read
			//load mdr with read pmem
			D_mdr_load = 1'b1;

			L2_pmem_read = 1'b1;
		end
		S_D_read3:
		begin
			//signal success
			D_pmem_resp = 1'b1;
		end
		S_D_write1:
		begin
			//load mar with D addr
			D_mar_load = 1'b1;
			//load mdr with D data
			D_cache_MDR_mux_sel = 1'b1;
			D_mdr_load = 1'b1;
		end
		S_D_write2:
		begin
			//write using D_MAR address
			L2_addressmux_sel = 1'b0;
			//write
			L2_pmem_write = 1'b1;
		end
		S_D_write3:
		begin
			//signal success
			D_pmem_resp = 1'b1;
		end
		S_D_last:
		begin
		end
	endcase
end

//state transitions for signals
always_comb
begin : next_state_logic
	next_state = state;
	case(state)
		S_I_read1:
		begin
			next_state = S_I_read2;
		end
		S_I_read2:
		begin
			if (L2_pmem_resp)
				next_state = S_I_read3;
			else
				next_state = state;
		end
		S_I_read3:
		begin
			if (I_pmem_read)
				next_state = state;
			else
				next_state = S_I_last;
		end
		S_I_last:
		begin
			if (D_pmem_read)
				next_state = S_D_read1;
			else if (D_pmem_write)
				next_state = S_D_write1;
			else if (I_pmem_read)
				next_state = S_I_read1;
			else
				next_state = state;
		end
		S_D_read1:
		begin
			next_state = S_D_read2;
		end
		S_D_read2:
		begin
			if (L2_pmem_resp)
				next_state = S_D_read3;
			else
				next_state = state;
		end
		S_D_read3:
		begin
			if (D_pmem_read)
				next_state = state;
			else
				next_state = S_D_last;
		end
		S_D_write1:
		begin
			next_state = S_D_write2;
		end
		S_D_write2:
		begin
			if (L2_pmem_resp)
				next_state = S_D_write3;
			else
				next_state = state;
		end
		S_D_write3:
		begin
			if (D_pmem_write)
				next_state = state;
			else
				next_state = S_D_last;
		end
		S_D_last:
		begin
			if (I_pmem_read)
				next_state = S_I_read1;
			else if (D_pmem_read)
				next_state = S_D_read1;
			else if (D_pmem_write)
				next_state = S_D_write1;
			else
				next_state = state;
		end
	endcase
end

//transition next state
always_ff @(posedge clk)
begin: next_state_assignment
	state <= next_state;
end

endmodule