import lc3b_types::*;

module indirect_state
(
	input logic clk,
	input lc3b_opcode opcode,
	input logic resp_b,
	
	output logic indirect_state_out
);

logic indirect_state;

initial
begin
	indirect_state = 1'b0;
end

always_ff @(posedge clk)
begin
//	indirect_state = (indirect_state_out == 1'b0) && (opcode == op_ldi); // TODO: verify / possibly fix this logic (look at a state diagram and the datapath)
	if (indirect_state_out == 1'b0) begin
		if ((opcode == op_ldi) || (opcode == op_sti)) begin
			indirect_state = 1'b1;
		end
		else begin
			indirect_state = 1'b0;
		end
	end
	else begin // indirect_state_out == 1'b1
		if (resp_b == 1'b1) begin
			indirect_state = 1'b0;
		end
		else begin
			indirect_state = 1'b1;
		end
	end
end

always_comb
begin
	indirect_state_out = indirect_state;
end

endmodule : indirect_state