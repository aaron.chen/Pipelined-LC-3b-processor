import lc3b_types::*;

module arbiter_datapath
(
	input clk,

	input lc3b_word I_pmem_address,
	output lc3b_c_line I_pmem_rdata,

	input lc3b_word D_pmem_address,
	input lc3b_c_line D_pmem_wdata,
	output lc3b_c_line D_pmem_rdata,

	input lc3b_c_line L2_pmem_rdata,

	output lc3b_c_line L2_pmem_wdata,
	output lc3b_word L2_pmem_address,

	input logic L2_addressmux_sel,
	input logic D_cache_MDR_mux_sel,

	input logic I_mar_load,
	input logic I_mdr_load,

	input logic D_mar_load,
	input logic D_mdr_load
);

lc3b_word I_cache_MAR_out;
lc3b_word D_cache_MAR_out;
lc3b_c_line I_cache_MDR_out;
lc3b_c_line D_cache_MDR_out;

lc3b_word L2_addressmux_out;
lc3b_c_line D_cache_MDR_mux_out;

register #(.width(lc3b_c_wordwidth)) I_cache_MAR
(
	.clk(clk),
	.load(I_mar_load),
	.in(I_pmem_address),
	.out(I_cache_MAR_out)
);

register #(.width(lc3b_c_linewidth)) I_cache_MDR
(
	.clk(clk),
	.load(I_mdr_load),
	.in(L2_pmem_rdata),
	.out(I_cache_MDR_out)
);

register #(.width(lc3b_c_wordwidth)) D_cache_MAR
(
	.clk(clk),
	.load(D_mar_load),
	.in(D_pmem_address),
	.out(D_cache_MAR_out)
);

register #(.width(lc3b_c_linewidth)) D_cache_MDR
(
	.clk(clk),
	.load(D_mdr_load),
	.in(L2_pmem_rdata),
	.out(D_cache_MDR_out)
);

mux2to1 #(.width(lc3b_c_wordwidth)) L2_addressmux
(
	.sel(L2_addressmux_sel),
	.a1(I_cache_MAR_out),
	.a0(D_cache_MAR_out),
	.out(L2_addressmux_out)
);

mux2to1 #(.width(lc3b_c_linewidth)) D_cache_MDR_mux
(
	.sel(D_cache_MDR_mux_sel),
	.a1(D_pmem_wdata),
	.a0(L2_pmem_rdata),
	.out(D_cache_MDR_mux_out)
);

assign I_pmem_rdata = I_cache_MDR_out;
assign D_pmem_rdata = D_cache_MDR_out;

assign L2_pmem_wdata = D_cache_MDR_out;
assign L2_pmem_address = L2_addressmux_out;

endmodule