import lc3b_types::*;
//specialized register that holds fields needed for the DECODE stage from the FETCH stage
//also has special fields to indicate data fields
//on every clock cycle, if load == 1, internal data fields are assigned to the respective in fields
//output various data fields
module IF_ID_register
(
	input logic clk,
	input logic load,
	input logic squash,
	input lc3b_control_word ctrl_in,

	input lc3b_word instr_in,
	input lc3b_word pc_in,

	output lc3b_control_word ctrl_out,
	output lc3b_reg dr_out,
	output lc3b_reg sr1_out,
	output lc3b_reg sr2_out,

	output lc3b_imm5 imm5_out,
	output lc3b_imm4 imm4_out,

	output lc3b_offset11 off11_out,
	output lc3b_offset9 off9_out,
	output lc3b_offset6 off6_out,

	output lc3b_trap8 trap8_out,
	output lc3b_word pc_out

);

lc3b_IF_ID_instr_data instr_data; //only need 12 bits of the instruction
lc3b_word pc_data;
lc3b_control_word ctrl_data;

/* Altera device registers are 0 at power on. Specify this
 * so that Modelsim works as expected.
 */
initial
begin
	instr_data = 1'b0;
	pc_data = 1'b0;
	ctrl_data = 1'b0;
end

always @(posedge clk)
begin
	if (squash)
	begin
		instr_data = 12'b000000000000;
		pc_data = 16'b0000000000000000;
		ctrl_data = 0;
	end
	else if (load)
	begin
		instr_data = instr_in[11:0];
		pc_data = pc_in;
		ctrl_data = ctrl_in;
	end
end

always_comb
begin
	dr_out = instr_data[11:9];
	sr1_out = instr_data[8:6];
	sr2_out = instr_data[2:0];

	imm5_out = instr_data[4:0];
	imm4_out = instr_data[3:0];

	off11_out = instr_data[10:0];
	off9_out = instr_data[8:0];
	off6_out = instr_data[5:0];

	trap8_out = instr_data[7:0];

	pc_out = pc_data;
	ctrl_out = ctrl_data;
end

endmodule : IF_ID_register
