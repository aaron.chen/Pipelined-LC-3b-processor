//returns 1 if inputs equal, else 0
module comparator #(parameter width = 16)
(
	input logic [width-1:0] a, b,
	output logic out
);

always_comb
begin
	if (a == b)
		out = 1'b1;
	else
		out = 1'b0;
end

endmodule : comparator