import lc3b_types::*;
//specialized register that holds fields needed for the MEMORY stage from the EXECUTE stage
//also has special fields to indicate data fields
//on every clock cycle, if load == 1, internal data fields are assigned to the respective in fields
//output various data fields
module EX_MEM_register
(
	input logic clk,
	input logic load,
	input logic squash,
	input lc3b_control_word ctrl_in,

	input lc3b_word alu_in, 
	input lc3b_word regfile_sr_in, // for use in str
	input lc3b_reg dr_nzp_in, //for use in ldr or in branch comp
	input lc3b_trap8 trap8_in,

	output lc3b_control_word ctrl_out,
	output lc3b_word alu_out, 
	output lc3b_word regfile_sr_out, // for use in str
	output lc3b_reg dr_nzp_out, //for use in br
	output lc3b_trap8 trap8_out

);

lc3b_word alu_data; 
lc3b_word regfile_sr_data; // for use in str
lc3b_reg dr_nzp_data; //for use in br
lc3b_trap8 trap8_data;
lc3b_control_word ctrl_data;

/* Altera device registers are 0 at power on. Specify this
 * so that Modelsim works as expected.
 */
initial
begin
	alu_data = 1'b0;
	regfile_sr_data = 1'b0;
	dr_nzp_data = 1'b0; 
	trap8_data = 1'b0;
	ctrl_data = 1'b0;
end

always @(posedge clk)
begin
	if (squash)
	begin
		regfile_sr_data = 0;
		alu_data = 0;
		dr_nzp_data = 0;
		trap8_data = 0;
		ctrl_data = 0;
	end
	else
	begin
		if (load && (ctrl_data.opcode != op_sti))
		begin
			regfile_sr_data = regfile_sr_in;
		end
		if (load)
		begin
			alu_data = alu_in;
	//		regfile_sr_data = regfile_sr_in;
			dr_nzp_data = dr_nzp_in;
			trap8_data = trap8_in;
			ctrl_data = ctrl_in;
		end
	end
end

always_comb
begin
	alu_out = alu_data;
	regfile_sr_out = regfile_sr_data;
	dr_nzp_out = dr_nzp_data;
	trap8_out = trap8_data;
	ctrl_out = ctrl_data;
end

endmodule : EX_MEM_register
