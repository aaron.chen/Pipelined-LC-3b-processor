package lc3b_types;

//cpu datapath
typedef logic [15:0] lc3b_word;
typedef logic  [7:0] lc3b_byte;

typedef logic  [11:0] lc3b_IF_ID_instr_data;

typedef logic  [10:0] lc3b_offset11;
typedef logic  [8:0] lc3b_offset9;
typedef logic  [5:0] lc3b_offset6;
typedef logic  [4:0] lc3b_imm5;
typedef logic  [3:0] lc3b_imm4;
typedef logic  [1:0] lc3b_shf_ad;
typedef logic  [0:0] lc3b_shf_a;
typedef logic  [0:0] lc3b_shf_d;
typedef logic  [0:0] lc3b_imm_sel;
typedef logic  [0:0] lc3b_jsr_sel;

typedef logic  [7:0] lc3b_trap8;

typedef logic  [2:0] lc3b_reg;
typedef logic  [2:0] lc3b_nzp;
typedef logic  [1:0] lc3b_mem_wmask;

//cache datapath
parameter int lc3b_c_arrlen = 8;
parameter int lc3b_c_wordwidth = 16;
parameter int lc3b_c_linewidth = 128;

typedef logic [lc3b_c_linewidth-1:0] lc3b_c_line;
typedef logic [2:0] lc3b_c_index;
parameter int lc3b_c_taglen = 9;
typedef logic [lc3b_c_taglen-1:0] lc3b_c_tag;

//booleans
typedef enum bit {
	true = 1'b1,
	false = 1'b0
} booleans;

typedef enum bit [3:0] {
	op_add  = 4'b0001,
	op_and  = 4'b0101,
	op_br   = 4'b0000,
	op_jmp  = 4'b1100,   /* also RET */
	op_jsr  = 4'b0100,   /* also JSRR */
	op_ldb  = 4'b0010,
	op_ldi  = 4'b1010,
	op_ldr  = 4'b0110,
	op_lea  = 4'b1110,
	op_not  = 4'b1001,
	op_rti  = 4'b1000,
	op_shf  = 4'b1101,
	op_stb  = 4'b0011,
	op_sti  = 4'b1011,
	op_str  = 4'b0111,
	op_trap = 4'b1111
} lc3b_opcode;

typedef enum bit [3:0] {
	alu_add,
	alu_and,
	alu_not,
	alu_pass,
	alu_sll,
	alu_srl,
	alu_sra
} lc3b_aluop;

//pipeline control word
typedef struct packed {
	//inputs to generate control word -> to run through control_rom
	lc3b_opcode opcode;
	lc3b_shf_a shf_a; // also used as the imm sel
	lc3b_shf_d shf_d;
	lc3b_jsr_sel jsr_sel;

	//outputs to stages -> calculated with control_rom
	// decode
	logic storemux_sel;
	logic operand1mux_sel;
	logic [2:0] operand2mux_sel;
	// execute
	lc3b_aluop aluop;
	// memory access / load registers / branch?
	logic marmux_sel;
	logic [1:0] mdrmux_sel;
	logic read_b;
	logic write_b;
	lc3b_mem_wmask wmask_b;
	// writeback
	logic load_regfile;
	logic load_cc;
	logic [1:0] datawritemux_sel;
	logic drmux_sel; // TODO(optimization): drmux_sel, inmux_sel, and regfile_loadmux_sel will always be the same value (?)
	logic inmux_sel;
	logic load_regfilemux_sel;
	logic wmaskmux_sel;
} lc3b_control_word;

endpackage : lc3b_types
