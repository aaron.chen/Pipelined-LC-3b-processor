import lc3b_types::*;
//specialized register that holds fields needed for the MEMORY stage from the EXECUTE stage
//also has special fields to indicate data fields
//on every clock cycle, if load == 1, internal data fields are assigned to the respective in fields
//output various data fields
module MEM_WB_register
(
	input logic clk,
	input logic load,
	input logic squash,
	input lc3b_control_word ctrl_in,
	input logic resp_b,

	input lc3b_word alu_in, 
	input lc3b_word mem_in, // for use in str
	input lc3b_reg dr_nzp_in, //for use in ldr

	output lc3b_control_word ctrl_out,
	output lc3b_word alu_out, 
	output lc3b_word mem_out, // for use in str
	output lc3b_reg dr_nzp_out //for use in ldr or in branch 

);

lc3b_word alu_data; 
lc3b_word mem_data; // for use in str
lc3b_reg dr_nzp_data; //for use in ldr or in branch comp
lc3b_control_word ctrl_data;

/* Altera device registers are 0 at power on. Specify this
 * so that Modelsim works as expected.
// */
initial
begin
	alu_data = 1'b0;
	mem_data = 1'b0;
	dr_nzp_data = 1'b0; 
	ctrl_data = 1'b0; 
end

always @(posedge clk)
begin
	if (squash)
	begin
		mem_data = 0;
		alu_data = 0;
		dr_nzp_data = 0;
		ctrl_data = 0;
	end
	else
	begin
		if (resp_b)
		begin
			mem_data = mem_in; // always load mem_data in order for LDI to function properly without needing another register
		end

		if (load)
		begin
			alu_data = alu_in;
			dr_nzp_data = dr_nzp_in;
			ctrl_data = ctrl_in;
		end
	end
end

always_comb
begin
	alu_out = alu_data;
	mem_out = mem_data;
	dr_nzp_out = dr_nzp_data;
	ctrl_out = ctrl_data;
end

endmodule : MEM_WB_register
