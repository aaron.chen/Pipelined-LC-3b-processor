/*
Write-back means that you should only flush a dirty cache line to the memory when you replace it with some other line. This contrasts with write through caches which write to memory on every single cache write.
Write allocate means that you should bring a cache line into the cache upon cache miss while attempting to perform a write, rather than simply writing to the main memory.

You should not be writing back if there is a hit. A writeback (writing a cache line to physical memory) should only be carried out if there is a cache miss, and the least recently used cache line in the corresponding set is dirty.
*/

import lc3b_types::*;

module L2_cache_controller
(
	input clk,

	//Cache Controller -> CPU datapath
	output logic mem_resp,
	//CPU datapath -> Cache Controller
	input logic mem_read,
	input logic mem_write,

	//Cache Controller -> Main Memory or lower cache
	output logic pmem_read,
	output logic pmem_write,
	//Main Memory or lower cache -> Cache Controller
	input logic pmem_resp,

	//Cache -> Cache Controller
	input logic valid_bit_arr1_out,
	input logic valid_bit_arr0_out,
	input logic dirty_bit_arr1_out,
	input logic dirty_bit_arr0_out,
	input logic lru_bit_arr_out,
	input logic tag1comp_out,
	input logic tag0comp_out,

	input logic proper_address,


	//Cache Controller -> Cache

	output logic increment_miss,
	output logic increment_hit,

	output logic cacheinmux_sel,
	output logic cacheoutmux_sel,

	output logic valid_bit_arr1_write,
	output logic valid_bit_arr0_write,
	output logic dirty_bit_arr1_write,
	output logic dirty_bit_arr0_write,
	output logic dirty_bit_arr1_in,
	output logic dirty_bit_arr0_in,
	output logic lru_bit_arr_in,
	output logic lru_bit_arr_write,
	output logic tag_arr1_write,
	output logic tag_arr0_write,
	output logic data_arr1_write,
	output logic data_arr0_write
);

enum int unsigned
{
	hit,
	read_into_cache1,
	read_into_cache2,
	writeback
} state, next_state;

//define state actions for signals
always_comb
begin : state_actions
	//Cache Controller -> CPU datapath
	mem_resp = 1'b0;

	//Cache Controller -> Main Memory or lower cache
	pmem_read = 1'b0;
	pmem_write = 1'b0;

	//Cache Controller -> Cache
	cacheinmux_sel = 1'b0;
	cacheoutmux_sel = 1'b0;

	increment_hit = 1'b0;

	valid_bit_arr1_write = 1'b0;
	valid_bit_arr0_write = 1'b0;
	dirty_bit_arr1_in = 1'b0;
	dirty_bit_arr0_in = 1'b0;
	dirty_bit_arr1_write = 1'b0;
	dirty_bit_arr0_write = 1'b0;
	lru_bit_arr_in = 1'b0;
	lru_bit_arr_write = 1'b0;

	tag_arr1_write = 1'b0;
	tag_arr0_write = 1'b0;
	data_arr1_write = 1'b0;
	data_arr0_write = 1'b0;

	case(state)
		hit:
		begin
			if (mem_read && (tag1comp_out == 1'b1 && valid_bit_arr1_out == 1))
			begin
				cacheoutmux_sel = 1'b1;
				lru_bit_arr_in = 1'b0;
				lru_bit_arr_write = 1'b1;
				mem_resp = 1'b1;

				increment_hit = 1'b1;
			end
			else if (mem_read && (tag0comp_out == 1'b1 && valid_bit_arr0_out == 1))
			begin
				cacheoutmux_sel = 1'b0;
				lru_bit_arr_in = 1'b1;
				lru_bit_arr_write = 1'b1;
				mem_resp = 1'b1;

				increment_hit = 1'b1;
			end
			else if (mem_write && (tag1comp_out == 1'b1))
			begin
				cacheinmux_sel = 1'b0;

				lru_bit_arr_in = 1'b0;
				lru_bit_arr_write = 1'b1;
				dirty_bit_arr1_in = 1'b1;
				dirty_bit_arr1_write = 1'b1;
				data_arr1_write = 1'b1;
				mem_resp = 1'b1;

				increment_hit = 1'b1;
			end
			else if (mem_write && (tag0comp_out == 1'b1))
			begin
				cacheinmux_sel = 1'b0;

				lru_bit_arr_in = 1'b1;
				lru_bit_arr_write = 1'b1;
				dirty_bit_arr0_in = 1'b1;
				dirty_bit_arr0_write = 1'b1;
				data_arr0_write = 1'b1;
				mem_resp = 1'b1;

				increment_hit = 1'b1;
			end
			else
			begin
				mem_resp = 1'b0;
			end
		end
		read_into_cache1:
		begin
			pmem_read = 1'b1;
		end	
		read_into_cache2:
		begin
			cacheinmux_sel = 1'b1;
			if (lru_bit_arr_out == 1'b1)
			begin
				data_arr1_write = 1'b1;
				tag_arr1_write = 1'b1;

				valid_bit_arr1_write = 1'b1;
				dirty_bit_arr1_in = 1'b0;
				dirty_bit_arr1_write = 1'b1;
			end
			else
			begin
				data_arr0_write = 1'b1;
				tag_arr0_write = 1'b1;

				valid_bit_arr0_write = 1'b1;
				dirty_bit_arr0_in = 1'b0;
				dirty_bit_arr0_write = 1'b1;
			end
			pmem_read = 1'b1;
		end
		writeback:
		begin
			if (lru_bit_arr_out == 1'b1)
			begin
				cacheoutmux_sel = 1'b1;
			end
			else
			begin
				cacheoutmux_sel = 1'b0; 
			end
			pmem_write = 1'b1;
		end
	endcase // state
end

//state transitions for signals
always_comb
begin : next_state_logic
	next_state = hit;

	increment_miss = 1'b0;
	case(state)
		hit:
		begin
			if (proper_address == 1'b0)
			begin
				next_state = hit;
			end
			// on read / write request
			if (mem_read == 1 || mem_write == 1)
			begin
				//check if hit
				if (tag1comp_out == 1 && valid_bit_arr1_out == 1)
				begin
					next_state = hit;
				end
				else if (tag0comp_out == 1 && valid_bit_arr0_out == 1)
				begin
					next_state = hit;
				end
				else// if miss
				begin
					increment_miss = 1'b1;
					//"A writeback (writing a cache line to physical memory) should only
					//be carried out if there is a cache miss, and the least recently used
					//cache line in the corresponding set is dirty."
					if (lru_bit_arr_out == 1 && dirty_bit_arr1_out == 1)
					begin
						next_state = writeback;
					end
					else if (lru_bit_arr_out == 0 && dirty_bit_arr0_out == 1)
					begin
						next_state = writeback;
					end
					else
					begin
						next_state = read_into_cache1;
					end
				end
			end
			else
			begin
				next_state = hit;
			end
		end
		read_into_cache1:
		begin
			if (pmem_resp == 0)
			begin
				next_state = state;
			end
			else
			begin
				next_state = read_into_cache2;
			end
		end
		read_into_cache2:
		begin
			next_state = hit;
		end
		writeback:
		begin
			if (pmem_resp == 0)
			begin
				next_state = state;
			end
			else
			begin
				next_state = read_into_cache1;
			end
		end
	endcase // state
end

//transition next state
always_ff @(posedge clk)
begin: next_state_assignment
	state <= next_state;
end

endmodule : L2_cache_controller