import lc3b_types::*;

//takes 8-bit input, zexts it to 16-bit
module bytezext
(
	input lc3b_byte in,
	output lc3b_word out
);

assign out = $unsigned(in);

endmodule : bytezext