# Progress Reports

## Team Taro
Aaron Chen, William Miller, Jiayu Yao

## CP1 Progress Report
### Completed Since CP0:
- Started on Datapath
- Analyzed problems with initial datapath diagram
- Added control word type with necessary fields (load signals, MUX select signals, etc.)
- Implemented all basic modules (MUXes, ALU, regfile, etc.)
- Implemented control ROM which generates the control word based on the opcode and other bits from the instruction
- This module does not perform any computation
- Implemented a basic pipelined datapath that can handle the ADD, AND, NOT, LDR, STR, and BR instructions
- Pipeline is split into 5 stages: instruction fetch (IF), instruction decode (ID), execute (EX), memory access (MEM), and write back (WB)
- Pipeline includes stage registers between the stages
- Tested with test code
- DONE!

## CP2 Progress Report
### Completed Since CP1:
- Implemented full LC3b ISA
- Implemented cache arbiter
- Implemented split L1 caches and connected to arbiter
- Implemented multi-cycle L2 accesses

## CP3 Progress Report
### Completed Since CP2:
- A lot of code was lost when Jiayu's car was broken into Saturday night (18 Nov)
	- Will need to reimplement hazard detection and forwarding
- Still working on the unified L2 cache
- Finished design for static branch predictor

## CP4 Progress Report
### Completed Since CP3:
- Implemented data forwarding
- Implemented cache
- Implemented static branch predictor

#### Still to do:
- Implement hazard detection (CP3)
- Implement eviction write buffer (CP4)
- Implement performance counters (CP4)
