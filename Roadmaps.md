# Roadmaps

## Team Taro
Aaron Chen, William Miller, Jiayu Yao

## Roadmap to CP2

### By Monday (10/29) - Finish all LC3-b instructions
- Comment / Document code - Aaron
- Instructions implemented - William / Aaron
- Instructions tested - Aaron

### By Wednesday (11/1) - Cache Arbiter, L1 Cache, and data forwarding
- Finish connecting Cache Arbiter and L1 Cache - William / Jiayu
- Finish data forwarding design - Aaron

### By Friday (11/3) - L2 Cache
- Finish dummy L2 cache - Aaron
- Connecting L2 to everything else and testing - everyone 

## Roadmap to CP3

### By Wednesday (11/15) - L2 cache
- Verify L2 cache is completed and integerated into the cache hierarchy - Aaron

### By Friday (11/17) - Hazard detection and data forwarding
- Implement hazard detection - Aaron / Jiayu
- Implement data fowarding - William / Jiayu
- Test design and fix any bugs - everyone
- Branch prediction design, eviction write buffer, and performance counters design - everyone (likely one per person)

## Roadmap to CP4

Note: a correct implementation of the previous checkpoint should be completed before moving on to the features of checkpoint. Our group's progress for checkpoint 3 was hindered due to the event described in the progress report.

### By Wednesday (11/22) - Initial progress
- Write test code for software visible performance counters - Aaron
- Implement software visible performance counters - Aaron
- Begin work on an eviction write buffer - Jiayu
- Begin work a static branch prediction scheme - William

### By Saturday (11/25) - Write test code and complete implementation
Per the documentation, this checkpoint requires that test code be written to demonstrate the features required in this checkpoint.
- Write test code for the eviction write buffer - Jiayu / Aaron
- Write test code for the static branch prediciton scheme - William
- Complete implementation of an eviction write buffer - Jiayu / Aaron
- Complete implementation of a static branch prediction scheme - William
- Test design and fix any bugs - everyone 

## Roadmap to CP5

Note: as in CP4, a correct implementation of checkpoint 4 should be completed before moving on to the features of checkpoint 5.

The exact due date for the report has not been posted yet. Reasonable progress should be made on the report at least 2 days before it is due.

### By Tuesday (11/28) - Decide on features
- Discuss which features to implement out of the advanced design options - Will / Aaron
- Volunteer for specific options - Will / Aaron

### By Thursday (11/30) - Begin work on features
- Have a detailed design for each feature - Will / Aaron
- Begin work on implementation of each feature - Will / Aaron

### By Saturday (12/2) - Complete features and work on presentation and report
- Write tests for each feature - Will / Aaron
- Complete implementation of each feature - Will / Aaron
- Fix any bugs on all of the features - Will / Aaron
- Complete a rough draft of the presentation - Will / Aaron
- Complete a rough draft of the report - Will / Aaron

### By Monday (12/4) - Complete presentation
- Complete final draft of presentation - Will / Aaron
- Volunteer to talk for different slides - Will / Aaron
