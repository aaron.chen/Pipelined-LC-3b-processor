SEGMENT  CodeSegment:

   LDB  R1, R0, NEGTWO  ; R1 <= 0xfeff & 0x00ff (LC3 is little endian) ; 0x00
   LDB  R2, R0, H1234   ; R2 <= 0x3412 & 0x00ff ; 0x02
   LDB  R3, R0, 4x17    ; R3 <= 0x34 ; 0x04
   NOP ; 0x06
   NOP ; 0x08
   NOP ; 0x0A
   NOP ; 0x0C
   NOP ; 0x0E
   NOP ; 0x10
   NOP ; 0x12
   BRnzp DONE ; 0x14

ONE:    DATA2 4x0001 ; 0x16
H1234:  DATA2 4x1234 ; 0x18
NEGTWO: DATA2 4xFFFE ; 0x1A
TEMP1:  DATA2 4x0001 ; 0x1C
GOOD:   DATA2 4x600D ; 0x1E
BADD:   DATA2 4xBADD ; 0x20

DONE:
   LDR  R1, R0, GOOD
   BRnzp DONE
   NOP
   NOP
   NOP
   NOP
   NOP
   NOP
   NOP
