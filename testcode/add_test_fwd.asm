SEGMENT  CodeSegment:

   LDR  R1, R0, THREE ; R1 <= 0x1234 ; 0x00
   NOP ; 0x02
   NOP ; 0x04
   NOP ; 0x06
   NOP ; 0x08
   NOP ; 0x0A
   NOP ; 0x0C
   NOP ; 0x0E
   LDR  R2, R0, FIVE ; R1 <= 0x1234 ; 0x10
   NOP ; 0x12
   NOP ; 0x14
   NOP ; 0x16
   NOP ; 0x18
   NOP ; 0x1A
   NOP ; 0x1C
   NOP ; 0x1E
   ADD R3, R1, R2 ; 0x20
   ADD R4, R3, R1; 0x22
   BRnzp DONE ; 0x24

THREE:  DATA2 4x0003 ; 0x26
FIVE:   DATA2 4x0005 ; 0x28

DONE:
   BRnzp DONE
   NOP
   NOP
   NOP
   NOP
   NOP
   NOP
   NOP
