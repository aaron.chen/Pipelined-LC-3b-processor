SEGMENT  CodeSegment:

   LEA  R1, NEGTWO  ; R1 <= 0x1A ; 0x00
   LEA  R2, H1234   ; R2 <= 0x18 ; 0x02
   LEA  R3, ONE     ; R3 <= 0x16 ; 0x04
   NOP ; 0x06
   NOP ; 0x08
   NOP ; 0x0A
   NOP ; 0x0C
   NOP ; 0x0E
   NOP ; 0x10
   NOP ; 0x12
   BRnzp DONE ; 0x14

ONE:    DATA2 4x0001 ; 0x16
H1234:  DATA2 4x1234 ; 0x18
NEGTWO: DATA2 4xFFFE ; 0x1A
GOOD:   DATA2 4x600D ; 0x1C

DONE:
   LDR  R1, R0, GOOD
   BRnzp DONE
   NOP
   NOP
   NOP
   NOP
   NOP
   NOP
   NOP
