SEGMENT  CodeSegment:

   LDI  R1, R0, ADDR  ; R1 <= M[M[ADDR]] ; 0x00
   NOP ; 0x02
   NOP ; 0x04
   NOP ; 0x06
   NOP ; 0x08
   NOP ; 0x0A
   NOP ; 0x0C
   NOP ; 0x0E
   NOP ; 0x10
   NOP ; 0x12
   BRnzp DONE ; 0x14
   NOP ; 0x16
   NOP ; 0x18
   NOP ; 0x1A
   NOP ; 0x1C
   NOP ; 0x1E
   NOP ; 0x20
   NOP ; 0x22
   NOP ; 0x24
   NOP ; 0x26

ONE:    DATA2 4x0001 ; 0x28
H1234:  DATA2 4x1234 ; 0x2A
ADDR:   DATA2 4x0030 ; 0x2C
TEMP1:  DATA2 4x0001 ; 0x2E
GOOD:   DATA2 4x600D ; 0x30
BADD:   DATA2 4xBADD ; 0x32

DONE:
   BRnzp DONE
   NOP
   NOP
   NOP
   NOP
   NOP
   NOP
   NOP
