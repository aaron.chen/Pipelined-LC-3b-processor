SEGMENT  CodeSegment:

   LDR  R1, R0, NEGTWO  ; R1 <= -2 ; 0x00
   LDR  R2, R0, TWO     ; R2 <= 2 ; 0x02
   LDR  R4, R0, ONE     ; R4 <= 1 ; 0x04
   LDR  R3, R0, ADDR    ; R3 <= 0x30 ; 0x06
   NOP ; 0x08
   NOP ; 0x0A
   NOP ; 0x0C
   NOP ; 0x0E
   NOP ; 0x10
   NOP ; 0x12
   NOP ; 0x14
   JSRR R3 ; 0x16
   NOP ; 0x18
   NOP ; 0x1A
   NOP ; 0x1C
   NOP ; 0x1E
   NOP ; 0x20
   NOP ; 0x22
   NOP ; 0x24


ONE:    DATA2 4x0001 ; 0x26
TWO:    DATA2 4x0002 ; 0x28
NEGTWO: DATA2 4xFFFE ; 0x2A
TEMP1:  DATA2 4x0001 ; 0x2C
GOOD:   DATA2 4x600D ; 0x2E
BADD:   DATA2 4xBADD ; 0x30
ADDR:   DATA2 4x0034 ; 0x32

LOOP:
   ADD R3, R1, R2       ; R3 <= R1 + R2 ; 0x34
   AND R5, R1, R4       ; R5 <= R1 AND R4 ; 0x36
   NOT R6, R1           ; R6 <= NOT R1 ; 0x38
   NOP ; 0x3A
   NOP ; 0x3C
   NOP ; 0x3E
   NOP ; 0x40
   NOP ; 0x42
   NOP ; 0x44
   NOP ; 0x46
   STR R6, R0, TEMP1    ; M[TEMP1] <= R6 ; 0x48
   LDR R7, R0, TEMP1    ; R7 <= M[TEMP1] ; 0x4A
   ADD R1, R1, R4       ; R1 <= R1+1 ; 0x4C
   NOP ; 0x4E
   NOP ; 0x50
   NOP ; 0x52
   NOP
   NOP
   NOP
   NOP
   BRp DONE
   NOP
   NOP
   NOP
   NOP
   NOP
   NOP
   NOP
   BRnzp LOOP
   NOP
   NOP
   NOP
   NOP
   NOP
   NOP
   NOP

HALT:
   LDR  R1, R0, BADD
   BRnzp HALT
   NOP
   NOP
   NOP
   NOP
   NOP
   NOP
   NOP

DONE:
   LDR  R1, R0, GOOD
   BRnzp DONE
   NOP
   NOP
   NOP
   NOP
   NOP
   NOP
   NOP
