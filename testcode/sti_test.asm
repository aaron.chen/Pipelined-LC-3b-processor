SEGMENT  CodeSegment:

   LDR  R1, R0, H1234 ; R1 <= 0x1234 ; 0x00
   NOP ; 0x02
   NOP ; 0x04
   NOP ; 0x06
   NOP ; 0x08
   NOP ; 0x0A
   NOP ; 0x0C
   NOP ; 0x0E
   STI  R1, R0, ADDR  ; M[M[ADDR]] <= R1 ; 0x10
   NOP ; 0x12
   NOP ; 0x14
   NOP ; 0x16
   NOP ; 0x18
   NOP ; 0x1A
   NOP ; 0x1C
   NOP ; 0x1E
   NOP ; 0x20
   NOP ; 0x22
   BRnzp DONE ; 0x24
   NOP ; 0x26
   NOP ; 0x28
   NOP ; 0x2A
   NOP ; 0x2C
   NOP ; 0x2E
   NOP ; 0x30
   NOP ; 0x32
   NOP ; 0x34
   NOP ; 0x36

RESULT: DATA2 4xFFFF ; 0x38
H1234:  DATA2 4x1234 ; 0x3A
ADDR:   DATA2 4x0038 ; 0x3C
TEMP1:  DATA2 4x0001 ; 0x3E
GOOD:   DATA2 4x600D ; 0x40
BADD:   DATA2 4xBADD ; 0x42

DONE:
   BRnzp DONE
   NOP
   NOP
   NOP
   NOP
   NOP
   NOP
   NOP
