SEGMENT  CodeSegment:

   LDR  R1, R0, NEGTWO  ; R1 <= -2 ; 0x00
   LDR  R2, R0, NEGTWO  ; R2 <= -2 ; 0x02
   LDR  R3, R0, NEGTWO  ; R3 <= -2 ; 0x04
   NOP ; 0x06
   NOP ; 0x08
   NOP ; 0x0A
   NOP ; 0x0C
   NOP ; 0x0E
   NOP ; 0x10
   NOP ; 0x12
   BRnzp TEST ; 0x14
   NOP ; 0x16
   NOP ; 0x18
   NOP ; 0x1A
   NOP ; 0x1C
   NOP ; 0x1E
   NOP ; 0x20
   NOP ; 0x22


ONE:    DATA2 4x0001 ; 0x24
TWO:    DATA2 4x0002 ; 0x26
NEGTWO: DATA2 4xFFFE ; 0x28
TEMP1:  DATA2 4x0001 ; 0x2A
GOOD:   DATA2 4x600D ; 0x2C
BADD:   DATA2 4xBADD ; 0x2E

TEST:
   LSHF R1, R1, 7   ; R1 <= R1 << 7 ; 0x30
   RSHFL R2, R2, 7  ; R2 <= R2 >> 7,0 ; 0x32
   RSHFA R3, R3, 7  ; R3 <= R3 << 7,R3[15] ; 0x34
   NOP ; 0x36
   NOP ; 0x38
   NOP ; 0x3A
   NOP ; 0x3C
   NOP ; 0x3E
   NOP ; 0x40
   NOP ; 0x42
   BRnzp DONE
   NOP
   NOP
   NOP
   NOP
   NOP
   NOP
   NOP

DONE:
   LDR  R4, R0, GOOD
   BRnzp DONE
   NOP
   NOP
   NOP
   NOP
   NOP
   NOP
   NOP
