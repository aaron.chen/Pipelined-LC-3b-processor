SEGMENT  CodeSegment:

   LDR  R1, R0, ADDR  ; R1 <= -2 ; 0x00
   LDR  R2, R0, TWO     ; R2 <= 2 ; 0x02
   LDR  R4, R0, ONE     ; R4 <= 1 ; 0x04
   NOP ; 0x06
   NOP ; 0x08
   NOP ; 0x0A
   NOP ; 0x0C
   NOP ; 0x0E
   NOP ; 0x10
   NOP ; 0x12
   JMP R1 ; 0x14
   NOP ; 0x16
   NOP ; 0x18
   NOP ; 0x1A
   NOP ; 0x1C
   NOP ; 0x1E
   NOP ; 0x20
   NOP ; 0x22


ONE:    DATA2 4x0001 ; 0x24
TWO:    DATA2 4x0002 ; 0x26
ADDR:   DATA2 4x0030 ; 0x28
TEMP1:  DATA2 4x0001 ; 0x2A
GOOD:   DATA2 4x600D ; 0x2C
BADD:   DATA2 4xBADD ; 0x2E

LOOP:
   ADD R3, R1, R2       ; R3 <= R1 + R2 ; 0x30
   AND R5, R1, R4       ; R5 <= R1 AND R4 ; 0x32
   NOT R6, R1           ; R6 <= NOT R1 ; 0x34
   NOP ; 0x36
   NOP ; 0x38
   NOP ; 0x3A
   NOP ; 0x3C
   NOP ; 0x3E
   NOP ; 0x40
   NOP ; 0x42
   STR R6, R0, TEMP1    ; M[TEMP1] <= R6 ; 0x44
   LDR R7, R0, TEMP1    ; R7 <= M[TEMP1] ; 0x46
   ADD R1, R1, R4       ; R1 <= R1+1 ; 0x48
   NOP ; 0x4A
   NOP ; 0x4C
   NOP ; 0x4E
   NOP
   NOP
   NOP
   NOP
   BRp DONE
   NOP
   NOP
   NOP
   NOP
   NOP
   NOP
   NOP
   BRnzp LOOP
   NOP
   NOP
   NOP
   NOP
   NOP
   NOP
   NOP

HALT:
   LDR  R1, R0, BADD
   BRnzp HALT
   NOP
   NOP
   NOP
   NOP
   NOP
   NOP
   NOP

DONE:
   LDR  R1, R0, GOOD
   BRnzp DONE
   NOP
   NOP
   NOP
   NOP
   NOP
   NOP
   NOP
