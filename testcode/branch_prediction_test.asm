SEGMENT  CodeSegment:

   AND  R4, R0, R0
   BRp  DONE ; not taken
   LDR  R1, R0, THREE ; R1 <= 0x0003
   NOP
   NOP
   NOP
   NOP
   NOP
   NOP
   BRnzp DONE ; taken
   LDR  R2, R0, FIVE ; R2 <= 0x0005
   NOP
   NOP
   NOP
   NOP
   NOP
   NOP
   ADD R3, R1, R2
   NOP
   NOP
   NOP
   NOP
   NOP
   NOP
   ADD R4, R3, R1

THREE:  DATA2 4x0003
FIVE:   DATA2 4x0005

DONE:
   BRnzp DONE
   NOP
   NOP
   NOP
   NOP
   NOP
   NOP
   NOP
