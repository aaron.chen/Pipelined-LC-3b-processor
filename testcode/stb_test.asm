SEGMENT  CodeSegment:

   LDR  R1, R0, H1234   ; R1 <= 0x1234 ; 0x00
   LDR  R2, R0, H5678   ; R2 <= 0x5678 ; 0x02
   LDR  R3, R0, ADDR    ; R3 <= 0x002C ; 0x04
   NOP ; 0x06
   NOP ; 0x08
   NOP ; 0x0A
   NOP ; 0x0C
   NOP ; 0x0E
   NOP ; 0x10
   NOP ; 0x12
   BRnzp TEST ; 0x14
   NOP ; 0x16
   NOP ; 0x18
   NOP ; 0x1A
   NOP ; 0x1C
   NOP ; 0x1E
   NOP ; 0x20
   NOP ; 0x22


H1234:  DATA2 4x1234 ; 0x24
H5678:  DATA2 4x5678 ; 0x26
NEGTWO: DATA2 4xFFFE ; 0x28
ADDR:   DATA2 4x002C ; 0x2A
RES1:   DATA2 4xFFFF ; should be 0x7834 ; 0x2C
RES2:   DATA2 4xFFFF ; should be unchanged ; 0x2E
RES3:   DATA2 4xFFFF ; should be 0x78FF ; 0x30
RES4:   DATA2 4xFFFF ; should be unchanged ; 0x32

TEST:
   STB R1, R3, 0    ; mem[R3] <= R1[7:0] ; 0x34
   NOP ; 
   NOP ; 
   NOP ; 
   NOP ; 
   NOP ; 
   NOP ; 
   NOP ; 
   STB R2, R3, 1    ; mem[R3 + 1] <= R2[7:0] ; 
   NOP ; 
   NOP ; 
   NOP ; 
   NOP ; 
   NOP ; 
   NOP ; 
   NOP ; 
   STB R2, R3, 5    ; mem[R3 + 5] <= R2[7:0] ; 
   NOP ; 
   NOP ; 
   NOP ; 
   NOP ; 
   NOP ; 
   NOP ; 
   NOP ; 
   NOP ; 
   NOP ; 
   NOP ; 
   NOP ; 
   NOP ; 
   NOP ; 
   NOP ; 
   BRnzp DONE
   NOP
   NOP
   NOP
   NOP
   NOP
   NOP
   NOP

DONE:
   LDR  R1, R0, RES1
   LDR  R2, R0, RES2
   LDR  R3, R0, RES3
   LDR  R4, R0, RES4
   BRnzp DONE
   NOP
   NOP
   NOP
   NOP
   NOP
   NOP
   NOP
